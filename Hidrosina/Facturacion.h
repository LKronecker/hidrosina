//
//  Facturacion.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumEmergencia.h"
#import "InfoVehicular.h"

@class NumEmergencia, InfoVehicular, Rendimiento;

@interface Facturacion : UIViewController{
    NumEmergencia *emer;
    InfoVehicular *info;
    Rendimiento *rendi;
}

@property (nonatomic, retain) NumEmergencia *emer;
  @property (nonatomic, retain) InfoVehicular *info;
   @property (nonatomic, retain) Rendimiento *rendi;

-(IBAction)pushEmer:(id)sender;
-(IBAction)pushInfo:(id)sender;
-(IBAction)PushRend:(id)sender;
@end
