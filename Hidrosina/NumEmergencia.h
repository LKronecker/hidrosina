//
//  NumEmergencia.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-14.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NumEmergencia : UIViewController {
    
}

-(IBAction)llamar;
-(IBAction)llamar1;
-(IBAction)llamar2;
-(IBAction)llamar3;
-(IBAction)llamar4;
-(IBAction)llamar5;
-(IBAction)gotoBack:(id)sender;
@end
