//
//  HSDataStore.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-28.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import "HSDataStore.h"


@interface HSDataStore()

@property (nonatomic, strong) NSMutableArray * controllers;

@end

@implementation HSDataStore

#pragma mark - Singleton
static HSDataStore *_sharedDatastore = nil;

+ (void)initialize { // initialized is run by default by the framework before any message is sent to the specific class.
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDatastore = [[HSDataStore alloc] initSingleton];
    });
}

+ (id)shared {
    return _sharedDatastore;
}

- (id)initSingleton {
    self = [super init];
    if (self) {
        if (self.controllers == nil) {
            self.controllers = [NSMutableArray new];
        }
    }
    return self;
}

#pragma mark - Controller Management
//- (void)registerController:(id <HSDatastoreDelegate>)controller {
//    
//    if (![self.controllers containsObject:controller]) {
//        [self.controllers addObject:controller];
//    }
//    
//    // TODO report any potential API error messages to just-registered controllers here
//
//}




@end
