//
//  HSPlaces.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2015-03-15.
//  Copyright (c) 2015 Leopoldo G Vargas. All rights reserved.
//

#import "HSPlaces.h"

@implementation HSPlaces

- (NSArray *) tittles {
    
    NSArray *ttl = [[NSArray alloc]initWithObjects:
                    @"Camino Real A Toluca No. 521",@"Insurgentes Sur No. 2075",@"Alfonso XIII y Xola No. 18",@"Ajusco y Tlalpan No. 1395",@"Alfonso XIII y Obrero Mundial",@"Miguel A. y Murillo No. 21",@"Luz Saviñon y Div. del Norte No. 804",@"Av. Coyoacán No.107",@"Div. del Norte E Insurgentes Sur No.553",@"Calz. de Tlalpan No. 842",@"Dr. Vertiz No. 640",@"Av. Universidad No. 1291",@"Centenario No. 13",@"Cuauhtémoc No. 187",@"Eje Lázaro Cárdenas No. 199",@"Nuevo León No. 8",@"Cedro No. 238",@"Campeche No. 214",@"Guerrero No.284",@"Yucatán No. 125",@"Calzada Guadalupe No. 52",@"Av. Chapultepec y Veracruz No.2",@"Morelia y Puebla No. 137",@"Insurgentes y Popocatepetl No. 289",@"Av. Insurgentes Sur No. 541",@"Villalongín No. 6",@"Barcelona No.37",@"Guillermo Prieto No. 130",@"Fray Servando Teresa de Mier No. 299",@"Montevideo No. 2",@"Necaxa, Esq. Calzada de Guadalupe No. 516",@"Av. Instituto Politécnico Nacional No. 1881",@"Calzada Vallejo No. 1055",@"Calzada de Guadalupe No. 312",@"Plutarco Elías Calles No.1",@"Plutarco Elías Calles no. 1022",@"Coruña No. 452",@"Km. 17.5 Autopista México - Puebla",@"Calle Tolteca No. 69",@"Av. Ermita Iztapalapa No. 3389",@"Calz. Javier Rojo Gómez No. 462",@"Calz. Ermita Iztapalapa No. 3048",@"Calz. Ermita Iztapalapa No. 2710",@"Martí No. 210",@"turrigaray No. 116",@"Martí, Esq., Carlos B. Zetina No. 117",@"Lago Argentina No. 7",@"Protasio Tagle No. 2 y 4",@"Av. Río San Joaquín No. 5976",@"Mar Mediterráneo y Okostsk",@"Calz. Tulyehualco No. 5661",@"Fco. Morazán y General Anaya No. 175",@"José Joaquín Herrera No.110",
                    
                    @"Carr. Federal México - Puebla KM. 31",@"Av. De Los Maestros No. 47",@"Km. 42.+500 Carretera Federal Mex - Puebla",@"Av. Central No.20",@"Km. 31.5 Carretera México- Cuautitlan",@"Calz. de Las Armas No.17-A",@"Km 11.5 Carretera México - Pachuca",@"Av. Baja California  No.148",@"Boulevard del Lago No. 1-A",@"Av. Nezahualcoyotl No 172",@"Vía Adolfo López Mateos S/N",@"Calle Palomas",@"Vía José López Portillo No. 37",@"Av. Revolución S/N",@"Av. Gustavo Baz no. 4001",@"Vía Gustavo Baz No. 230",@"Vía Morelos No. 170",@"Av. Manuel de La Peña No. 148",
                    
                    @"Km 24.5 de Carretera Transpeninsular",@"Calle Julio Pimentel S/N",@"Leona Vicario S/N",@"Carretera Transpeninsular Km 35.7",@"Av. De las Brisas No. 3013",@"Boulevard Forjadores de Sudcalifornia s/n",@"Calle Manuel Abasolo S/N",@"Luis Donaldo Colosio S/n",@"Calle Prolongación Leona Vicario",@"Tercera Nte. y Segunda Ote. No.11",@"Av. Ejercito Nacional No.5230",@"Blvd. Oscar Tijerina No. 6751",@"L. J. Fuentes Rodríguez No. 595",@"Periférico Luis Echeverría No.999",@"Blvd. Torreón S/N",@"Blvd. Independencia Ote No.1100",@"Blvd Constitución #1111",@"Juan Pablo Rodríguez 730",@"Aguascalientes 1385",@"Magnolia No.100",@"Av. Heroico Colegio Militar No. 109",@"Blvd. Juan Alonso de Torres #3702",@"Avenida 12 de Octubre #200",@"Ave. Obregón Sur No.704",@"Av. Costera Miguel Alemán No. 379",@"Luis Donaldo Colosio No.1501",@"Blvd. Tula Iturbe # 118",@"Carretera México-Pachuca S/N",@"Boulevard Luis Donaldo Colosio no. 2003",@"Av. Hidalgo No.705",@"Av. Juárez No.850",@"Crucero de Tezoyuca Tepetzingo S/N",@"No Reelección 385",@"Km. 10.1 Carretera Federal México-Cuautla",@"Libramiento Cuautla-Izucar de Matamoros Km. 4+300 S/N",@"Libramiento Cuautla-Izucar de Matamoros Km. 4+300 S/N",@"Libramiento Cuautla-Izucar de Matamoros Km. 0.2 No.5",@"Libramiento Cuautla-Izucar de Matamoros Km. 5+050 S/N",@"Carretera Nacional No. 7877",@"Urano No.2620",@"Carretera Monterrey - Reynosa No.101",@"Carretera México- Tuxpan Km. 183.5",@"Héroes del 5 de Mayo No.1101",@"Av. Salvador Nava No.405",@"Emiliano Zapata Pte. No. 2151",@"Blvd. Benjamín Hill No.5720",@"Av. Patria, Esq. Blvd Luis F. Molina No.53140",@"Periférico Carlos Pellicer No.205",@"Ave. Carlos Pellicer S/N",@"Periférico Carlos Molina S/N",@"Carretera Federal a Paraíso No.420",@"Carretera Juárez Chiapas a Dos Bocas Paraíso S/N",@"Blvd. Hidalgo Km. 101 S/N",@"Av. Reforma S/N",@"Carretera Tampico Mante No. 6905",@"Blvd. Virreyes S/N",@"Vicente Guerrero No.900",@"Blvd. Puerto Industrial Primex No.105",@"Prol. González y Av. Leyes De Reforma No. 2035",@"Carretera Tlaxcala a Ocotlan #20",@"Autopista de cuota Arco Norte Km. 98+345.659",@"Autopista de cuota Arco Norte Km. 98+706.346",@"Avenida Puebla No.502",@"Blvd. Ruiz Cortinez No.1350",@"Av. Las Palmas Nte. No. 101",@"Blvd. Ávila Camacho #91",@"Ave. Oriente 31 y Norte 4 #263",////
                    @"Aeropuerto",@"Boulevard",@"Rastro",@"Armas",@"Chimalhuacán",@"Metex",@"Tultitlán",@"Perinorte",@"Perinorte",@"Peribásico",@"Doctores",@"Bolivar",@"Atizapán",@"Tollocan",@"Tula",@"Lago de Gpe",@"Alfredo del Mazo",@"Alpuyeca",@"Cuautitlán",@"Vía",@"Rojo Gómez",@"Metepec",@"Aguascalientes",@"Querétaro",@"Ixtlán",@"Nuevo Veracruz",@"Cuautitlán Izcalli",@"Córdoba",@"Tapachula",@"Tula Norte",@"Tula Sur",@" Medellín",@"Acaponeta 1",@"Acaponeta 2",@"Gustavo Baz II",@"Naucalpan",@"Central de Abastos",@"Mocambo",@"Querétaro",
                    
                    nil];
    
    return ttl;

}

- (NSArray *) gasStations {
    
    NSArray *gas = [[NSArray alloc]initWithObjects:
                    @"1",@"0",@"0",@"0",@"0",@"0",@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"0",@"0",@"1",@"0",@"0",@"0",@"0",@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"0",@"0",@"0",@"1",@"1",@"0",@"0",@"1",@"1",@"1",@"0",@"1",@"1",@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"1",@"1",@"0",
                    //
                    @"1",@"1",@"1",@"0",@"1",@"1",@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"1",@"1",@"1",
                    
                    @"1",@"0",@"0",@"1",@"1",@"1",@"1",@"1",@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"0",@"0",@"0",@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"1",@"1",@"0",@"0",@"0",@"0",@"0",  ///
                    @"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",
                    @"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",
                    @"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",
                    @"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",
                    @"1",@"1",@"1",
                    nil];

    return gas;

}

- (NSArray *) latitudes {

    NSArray *lat = [[NSArray alloc]initWithObjects:
                    @"19.390212",@"19.347666",@"19.394318",@"19.367536",@"19.402145",@"19.379434",@"19.392335",@"19.39716",@"19.399388",@"19.388557",@"19.397625",@"19.360324",@"19.350575",@"19.415706",@"19.446566",@"19.4155568",@"19.45128",@"19.408994",@"19.452316",@"19.412837",@"19.456452",@"19.420268",@"19.423993",@"19.415245",@"19.401137",@"19.431589",@"19.428546",@"19.438543",@"19.423044",@"19.485052",@"19.474917",@"19.495099",@"19.487613",@"19.467168",@"19.39717",@"19.388626",@"19.401602",@"19.357308",@"19.372504",@"19.346294",@"19.370565",@"19.343614",@"19.343281",@"19.401471",@"19.422217",@"19.40323",@"19.458006",@"19.416085",@"19.44551",@"19.459913",@"19.304334",@"19.427519",@"19.43846",
                    
                    @"19.314604",@"19.539192",@"19.352388",@"19.504675",@"19.65057",@"19.503237",@"19.52153",@"19.393967",@"19.607572",@"19.40926",@"19.530821",@"19.609727",@"19.635042",@"19.606836",@"19.58159",@"19.557397",@"19.583028",@"19.310351",
                    @"23.002276",@"23.151352",@"22.895770",@"23.086853",@"22.904792",@"24.105281",@"24.153159",@"24.135771",@"22.935531",@"16.23469",@"31.697420",@"31.659190",@"29.321015",@"25.455622",@"25.534774",@"25.557605",@"25.564479",@"25.464519",@"25.434526",@"24.042104",@"24.030698",@"21.140686",@"20.531425",@"20.562719",@"16.843548",@"20.098007",@"20.058305",@"19.831158",@"20.091299",@"20.677073",@"20.675126",@"18.802595",@"18.850324",@"18.894956",@"18.858872",@"18.796463",@"18.850280",@"18.817839",@"25.565807",@"25.683052",@"25.661864",@"20.137692",@"19.071427",@"22.140688",@"24.790420",@"24.748455",@"24.768341",@"17.971260",@"17.970406",@"17.998588",@"18.274346",@"18.069121",@"26.076051",@"27.457538",@"22.297527",@"26.086235",@"22.742264",@"22.395597",@"25.877302",@"19.317274",@"19.551121",@"19.551343",@"20.543039",@"19.161713",@"18.143298",@"17.994762",@"18.861788",///
                    //
                    @"19.310121",@"19.288649",@"19.317792",
                    @"19.471197",@"19.407975",@"19.357566",@"19.613325",@"19.544622",@"19.533884",@"19.604569",@"19.423684",@"19.427142",
                    
                    @"19.542714",@"19.287092",@"20.069003",@"19.620142",@"19.302670",@"18.756362",@"19.672277",@"19.559532",@"19.371266",@"19.255351",
                    
                    @"21.909224",@"20.652613",@"21.037230",@"19.178809",@"19.688645",@"18.882882",@"14.904024",@"20.095098",@"20.094088",@"19.089187",
                    @"22.442362",@"22.443174",@"19.585189",@"19.464698",@"19.375722",@"19.14104",@"20.64294",
                    
                    
                    
                    
                    nil];

    return lat;
}

- (NSArray *) longitudes {

    NSArray *log = [[NSArray alloc]initWithObjects:
                    @"-99.20505",@"-99.187755",@"-99.138714",@"-99.14225",@"-99.144552",@"-99.188213",@"-99.165591",@"-99.167293",@"-99.170389",@"-99.138434",@"-99.150643",@"-99.171627",@"-99.163799",@"-99.15476",@"-99.138691",@"-99.1699",@"-99.161963",@"-99.167772",@"-99.146083",@"-99.161184",@"-99.128752",@"-99.176316",@"-99.155957",@"-99.165733",@"-99.170339",@"-99.159623",@"-99.155203",@"-99.167945",@"-99.129459",@"-99.118939",@"-99.121399",@"-99.132494",@"-99.150969",@"-99.124335",@"-99.099316",@"-99.132601",@"-99.125331",@"-98.994524",@"-99.099048",@"-99.019337",@"-99.080967",@"-99.033414",@"-99.037785",@"-99.175644",@"-99.207251",@"-99.183052",@"-99.200356",@"-99.18245",@"-99.201309",@"-99.187037",@"-99.060239",@"-99.119989",@"-99.119214",
                    
                    @"-98.882173",@"-99.224165",@"-98.667119",@"-99.040053",@"-99.183479",@"-99.212275",@"-99.092722",@"-98.972088",@"-98.973499",@"-98.927092",@"-99.049599",@"-99.018338",@"-99.114406",@"-99.051557",@"-99.203241",@"-99.204432",@"-99.041281",@"-98.947479",
                    @"-109.733004",@"-109.709030",@"-109.916131",@"-109.707037",@"-109.938209",@"-110.311908",@"-110.324586",@"-110.332314",@"-109.931061",@"-93.897614",@"-106.411211",@"-106.440492",@"-100.956930",@"-101.009722",@"-103.394909",@"-103.436258",@"-103.443017",@"-100.978909",@"-100.999369",@"-104.629825",@"-104.642506",@"-101.633373",@"-100.836291",@"-101.198016",@"-99.911668",@"-98.761428",@"-99.334672",@"-98.979140",@"-98.754250",@"-103.353537",@"-103.357288",@"-99.197827",@"-99.178932",@"-99.166260",@"-98.959235",@"-98.916441",@"-98.935401",@"-98.918962",@"-100.239510",@"-100.485246",@"-100.152467",@"-98.097994",@"-98.193666",@"-100.955913",@"-107.425275",@"-107.426313",@"-107.370869",@"-92.923560",@"-92.971035",@"-93.386761",@"-93.218774",@"-93.164282",@"-98.315527",@"-99.511043",@"-97.876811",@"-98.301626",@"-98.962800",@"-97.931399",@"-97.521629",@"-98.221853",@"-98.494730",@"-98.493660",@"-97.467543",@"-96.108613",@"-94.473048",@"-94.541216",@"-97.108012",///
                    //
                    @"-99.561357",@"-99.688153",@"-99.630311",
                    
                    @"-99.223602",@"-98.924826",@"-98.986859",@"-99.186181",@"-99.211134",@"-99.219766",@"-99.189786",@"-99.152787",@"-99.139239",@"-99.233329",
                    
                    @"-99.557156",@"-99.220842",@"-99.211247",@"-99.627937",@"-99.242948",@"-99.226320",@"-99.047754",@"-99.080088",@"-99.619521",@"-102.317655",
                    
                    @"-100.472870",@"-104.286406",@"-96.187986",@"-99.226264",@"-96.922567",@"-92.247495",@"-99.29945",@"-99.299664",@"-96.153463",@"-105.416328",
                    
                    @"-105.415668",@"-99.20282",@"-99.218909",@"-99.097216",@"-96.116363",@"-100.459561",
                    
                    nil];

    return log;

}

- (NSArray *) subtittles {

    NSArray *stl = [[NSArray alloc]initWithObjects:
                    @"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium ",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",
                    
                    @"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",
                    
                    
                    @"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel ",@"Magna - Premium - Diesel ",@"Magna - Premium - Diesel ",@"Magna - Premium - Diesel ",@"Magna - Premium - Diesel ",@"Magna - Premium - Diesel ",@"Magna - Premium - Diesel ",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",////
                    
                    @"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna Diesel",@"Magna Diesel",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium",@"Magna - Premium - Diesel",@"Magna - Premium",@"Magna - Premium - Diesel",@" ",@"Magna - Premium - Diesel",@"Magna - Premium - Diesel",@"Magna - Premium",@" ",@"Magna - Premium",@" ",@" ",@" ",@" ",@" ",@" ",@" ",@" ",@" ",@" ",
                    
                    nil];

    return stl;
}

@end
