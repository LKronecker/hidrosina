//
//  MasterViewController.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"

@interface MasterViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MasterViewController

@synthesize autoArray, urlArray, urlSegArray, seguroArray, numArray, brandArray, picker, pickerArray, marca, seguro , yearArray, ano, myTextField, cancel, elegir, web;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Vehículo", @"Vehículo");
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            self.clearsSelectionOnViewWillAppear = NO;
            self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
        }
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.splitViewController.presentsWithGesture = NO;
    
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Barra-izquierda-1-1.png"]];
    
    cancel.alpha = 0;
        web.alpha = 0;
    elegir.alpha = 0;
   // self.tableView.alpha = 0.55;
    
    tableControl = 0;
    tableControl1 = 0;
    tableControl2 = 0;
    tableControl3 = 0;
    tableControl4 = 0;
  //  pickerCount = 1;
   // picker.alpha = 0;
    
    NSString *tc=[[NSUserDefaults standardUserDefaults] objectForKey:@"TC"];
    
    tableControl = tc;
    
    NSString *tc1=[[NSUserDefaults standardUserDefaults] objectForKey:@"TC1"];
    
    tableControl1 = tc1;
    
    NSString *tc2=[[NSUserDefaults standardUserDefaults] objectForKey:@"TC2"];
    
    tableControl2 = tc2;
    
    NSString *tc3=[[NSUserDefaults standardUserDefaults] objectForKey:@"TC3"];
    
    tableControl3 = tc3;
    
    NSString *tc4=[[NSUserDefaults standardUserDefaults] objectForKey:@"TC4"];
    
    tableControl3 = tc4;
    
    
       
      
    //[modelo setTitle:[[NSUserDefaults standardUserDefaults]
   //                   stringForKey:@"modelo"] forState:UIControlStateNormal];
   // [ano setTitle:[[NSUserDefaults standardUserDefaults]
     //              stringForKey:@"ano"] forState:UIControlStateNormal];
    
    NSArray *baseArray = [[NSArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
    self.pickerArray =baseArray;
    
    
    NSArray *info = [[NSArray alloc]initWithObjects:@"Número de póliza",@"------ Elegir ------",@"Marca del vehículo",@"------ Elegir ------",@"Modelo del vehículo",@"------ Elegir ------",@"Año del vehículo" ,@"------ Elegir ------",@"Aseguradora",@"------ Elegir ------",@"",@"",nil];
    
    self.autoArray =info;
    
    NSArray *year = [[NSArray alloc]initWithObjects:@"1990",@"1991",@"1992",@"1993",@"1994",@"1995",@"1996",@"1997",@"1998",@"1999",@"2000",@"2001",@"2002",@"2003",@"2004",@"2005",@"2006",@"2007",@"2008",@"2009",@"2010",@"2011",@"2012",@"2013", nil];
    
    self.yearArray =year;
    
    NSArray *phones = [[NSArray alloc]initWithObjects:@"018007122828",@"018000011300",@"018001111200",@"018009001292",@"018007093800",@"018004009000",@"018000115900",@"54903939",@"018008002021",@"91775000",@"018008327696",@"018002266783",@"018001122800",@"018009090000",nil];
    self.numArray = phones;
    
    
    
    NSArray *segs = [[NSArray alloc]initWithObjects:@"ABA Seguros",@"AIG M'exico",@"Allianx M'exico",@"AXA",@"El aguila",@"GNP",@"Grupo Zurich",@"Mapfre",@"Qualitas",@"Seguros Atlas",@"Seguros Banamex",@"Seguros Banorte",@"Seguros BBVA Bancomer",@"Seguros Inbursa"
                     , nil];
    self.seguroArray = segs;
    
    NSArray *urlSeg = [[NSArray alloc]initWithObjects:@"http://www.abaseguros.com/Paginas/default.aspx",@"http://www.aigmex.com",@"http://www.allianz.com.mx/home",@"http://axa.mx/Personas/AxaSeguros/default.aspx",@"http://www.elaguila.com.mx",@"https://www.gnp.com.mx",@"http://www.zurich.com.mx",@"http://www.mapfre.com.mx",@"http://www.qualitas.com.mx",@"http://www.segurosatlas.com.mx",@"http://www.segurosbanamex.com.mx",@"http://www.banorte.com/portal/personas/home.web",@"https://www.segurosbancomer.com.mx/seguros/default.asp",@"http://www.inbursa.com/index.asp",@"http://www.mnyl.com.mx", nil];
    self.urlSegArray =urlSeg;
    
    
    NSArray *urls = [[NSArray alloc]initWithObjects:@"http://www.ford.mx",@"http://www.chrysler.com.mx",
                     @"http://www.dodge.com.mx",@"http://www.nissan.com.mx",@"http://www.gmc.com.mx",
                     @"http://www.bmw.com.mx",@"http://www2.mercedes-benz.com.mx",
                     @"http://www.jeep.com.mx",@"http://www.camionesram.com.mx",@"http://www.fiat.com.mx",@"http://www.alfaromeo.mx",@"http://www.mitsubishi-motors.com.mx",@"http://www.acura.mx",@"http://www.honda.mx",@"http://www.mazdamexico.com.mx",@"http://www.toyota.com.mx",@"http://www.vw.com.mx",@"http://www.pontiac.com.mx",@"http://www.peugeot.com.mx",@"http://renault.com.mx",@"http://www.suzuki.com.mx",@"http://www.mini.com.mx",@"http://www.audi.com.mx", nil];
    
    self.urlArray =urls;
    
    NSArray *brands = [[NSArray alloc]initWithObjects:@"Ford",@"Chrysler",@"Dodge",@"Nissan",@"GMC",@"BMW",@"Mercedes Benz",@"Jeep",@"Ram",@"Fiat",@"Alfa Romeo",@"Mitsubishi", @"Acura",@"Honda",@"Mazda",@"Toyota",@"Volkswagen",@"Pontiac",@"Peugeot",@"Renault",@"Suzuki",@"Mini",@"Audi",  nil];
    
    self.brandArray = brands;

	// Do any additional setup after loading the view, typically from a nib.
  //  self.navigationItem.leftBarButtonItem = self.editButtonItem;

  //  UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
  //  self.navigationItem.rightBarButtonItem = addButton;
    
    //self.navigationController.navigationBar.tintColor = [UIColor greenColor];
   // self.navigationController.navigationBar.backgroundColor
    //self.tableView.alpha = 0.6;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
    [newManagedObject setValue:[NSDate date] forKey:@"timeStamp"];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
         // Replace this implementation with code to handle the error appropriately.
         // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

-(IBAction)cancel:(id)sender{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
   
    cancel.alpha = 0;
    web.alpha = 0;
    elegir.alpha = 0;
   picker.alpha =  0;
    [UIView commitAnimations];
    
    
}

-(IBAction)web:(id)sender{
    NSInteger row = [picker selectedRowInComponent:0];
    
    if(pickerCount == 0){
        NSString *carSt = [[NSString alloc]initWithFormat:@"%@", [self.urlArray objectAtIndex:row]];
        
       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:carSt]];
        
    }if(pickerCount == 1){
        NSString *carSt = [[NSString alloc]initWithFormat:@"%@", [self.urlSegArray objectAtIndex:row]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:carSt]];
        
        
    }    


}
-(IBAction)elegirMarca:(id)sender{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
       picker.alpha =  0;
    cancel.alpha = 0;
    web.alpha = 0;
    elegir.alpha = 0;
    
    
    [UIView commitAnimations];
    
    NSInteger row = [picker selectedRowInComponent:0];
    if(pickerCount == 0){
        NSString *carSt = [[NSString alloc]initWithFormat:@"%@", [self.brandArray objectAtIndex:row]];
                
        [[NSUserDefaults standardUserDefaults] setObject:carSt forKey:@"marca"];
        [self.tableView reloadData];
        [self.tableView reloadInputViews];

    }if(pickerCount == 1){
        NSString *carSt = [[NSString alloc]initWithFormat:@"%@", [self.seguroArray objectAtIndex:row]];
      
        [[NSUserDefaults standardUserDefaults] setObject:carSt forKey:@"poliza"];
        
        [self.tableView reloadData];
        [self.tableView reloadInputViews];
    }if(pickerCount == 2){
        NSString *carSt = [[NSString alloc]initWithFormat:@"%@", [self.yearArray objectAtIndex:row]];
        
        [[NSUserDefaults standardUserDefaults] setObject:carSt forKey:@"ano"];
        
        [self.tableView reloadData];
        [self.tableView reloadInputViews];
    }
    
    }
    


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   // return [[self.fetchedResultsController sections] count];
    
    return (1);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  //  id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [autoArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   //  UITableViewCell *cell = [[UITableViewCell alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 320, 180)];
    [picker setDelegate:self];
        picker.alpha = 0;
  ////////
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button1 addTarget:self
                action:@selector(elegirMarca:)
      forControlEvents:UIControlEventTouchDown];
    
    [button1 setBackgroundImage:[UIImage imageNamed:@"elegir.png"] forState:UIControlStateNormal];
    
    button1.frame = CGRectMake(0, 0, 106, 60);
    
    self.elegir = button1;
    
    /////
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button2 addTarget:self
                action:@selector(web:)
      forControlEvents:UIControlEventTouchDown];
    
    [button2 setBackgroundImage:[UIImage imageNamed:@"sitio-web.png"] forState:UIControlStateNormal];
    
    button2.frame = CGRectMake(106, 0, 106, 60);
    
    self.web = button2;
    /////
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button3 addTarget:self
                action:@selector(cancel:)
      forControlEvents:UIControlEventTouchDown];
    
    [button3 setBackgroundImage:[UIImage imageNamed:@"cancelar.png"] forState:UIControlStateNormal];
    
    button3.frame = CGRectMake(212, 0, 106, 60);

        self.cancel = button3;
    ////////
    if (indexPath.row == 0) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
        cell.imageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"Barra-izquierda-1-1.png"]];
    }
    if (indexPath.row == 1) {
        cell.frame = CGRectMake(0, 0, 320, 44);
       
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        if (tableControl3 ==0) {
            cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"noPoliz"];
            
        }

        
       
    }
    if (indexPath.row == 2) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
    }
    if (indexPath.row == 3) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if (tableControl ==0) {
            cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
        }else{
        cell.textLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"marca"];
     
        }
    }
    if (indexPath.row == 4) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
    }
    if (indexPath.row == 5) {
        cell.frame = CGRectMake(0, 0, 320, 44);
       // cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if (tableControl4 ==0) {
            cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"modelo"];
            
        }
    }
    if (indexPath.row == 6) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
    }
    if (indexPath.row == 7) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if (tableControl1 ==0) {
            cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"ano"];
            
        }

    }
    if (indexPath.row == 8) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
    }
    if (indexPath.row == 9) {
        cell.frame = CGRectMake(0, 0, 320, 44);
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if (tableControl2 ==0) {
            cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"poliza"];
            
        }

    }

    
        
    if (indexPath.row == 11) {
        
     //   cell.backgroundColor = [UIColor greenColor];
        // cell.textLabel.text = [autoArray objectAtIndex:1];
        
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Barra-izquierda-1-1.png"]];
      
        [cell addSubview:button1];
        [cell addSubview:button2];
        [cell addSubview:button3];
        
      
        
    }
    if (indexPath.row == 10) {
        
        cell.frame = CGRectMake(0, 0, 320, 180);
       // cell.imageView.frame = cgrectmake
        cell.imageView.image = [UIImage imageNamed:@"pin-640-x-360"];
        
        // cell.textLabel.text = [autoArray objectAtIndex:1];
        cell.alpha = 0;
        [cell addSubview:picker];
    }



/*    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       
    }
    
    picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 320, 180)];
    [picker setDelegate:self];
    

    if (indexPath.row == 10) {
        cell.frame = CGRectMake(0, 0, 320, 180);
    [cell addSubview:picker];
    }

   // [self configureCell:cell atIndexPath:indexPath];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [autoArray objectAtIndex:indexPath.row];
    
*/
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int i;
    if (indexPath.row == 0) i = 44;
    if (indexPath.row == 1) i = 44;
    if (indexPath.row == 2) i = 44;
    if (indexPath.row == 3) i = 44;
    if (indexPath.row == 4) i = 44;
    if (indexPath.row == 5) i = 44;
    if (indexPath.row == 6) i = 44;
    if (indexPath.row == 7) i = 44;
    if (indexPath.row == 8) i = 44;
    if (indexPath.row == 9) i = 44;
    
    if (indexPath.row == 11) i = 60;
    if (indexPath.row == 10) i = 180;
    
    return i;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    if (!self.detailViewController) {
	      //  self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
	    }
        //self.detailViewController.detailItem = object;
       // [self.navigationController pushViewController:self.detailViewController animated:YES];
    } else {
      //  self.detailViewController.detailItem = object;
        
        if(indexPath.row == 1){
             tableControl3 = 1;
            
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"TC3"];
            seleCount = 0;
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Núnero de Poliza", @"new_list_dialog")
                                                              message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
             myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
//        myTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
//        [myTextField setBackgroundColor:[UIColor whiteColor]];
//        [myAlertView addSubview:myTextField];
            [myAlertView show];
        }
        if(indexPath.row == 3){
        
            pickerCount = 0;
            picker.alpha = 1;
            
            tableControl = 1;
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"TC"];
            
            cancel.alpha = 1;
            web.alpha = 1;
            elegir.alpha = 1;
            
            [picker reloadAllComponents];
        }
        if(indexPath.row == 5){
            
            picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 100, 320, 180)];
            [picker setDelegate:self];
            /////
                     seleCount = 1;
            
            tableControl4 = 1;
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"TC4"];
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Modelo del Vehículo", @"new_list_dialog")
                                                                  message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
             myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
          //  myAlertView.frame = CGRectMake(0, 0, 520, 380);
//            myTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
//            [myTextField setBackgroundColor:[UIColor whiteColor]];
//            [myAlertView addSubview:myTextField];

            [myAlertView show];
        }

        if(indexPath.row == 7){
            
            pickerCount = 2;
            picker.alpha = 1;
                        tableControl1 = 1;
             [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"TC1"];
            
            cancel.alpha = 1;
            web.alpha = 1;
            elegir.alpha = 1;
            
            
            [picker reloadAllComponents];
            
            /////
       /*     picker = [[UIPickerView alloc]initWithFrame:CGRectMake(-15, 100, 320, 180)];
            [picker setDelegate:self];
            /////
            seleCount = 1;
            tableControl4 = 1;
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Año del Vehículo", @"new_list_dialog")
                                                                  message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            //  myAlertView.frame = CGRectMake(0, 0, 520, 380);
          //  myTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
           // [myTextField setBackgroundColor:[UIColor whiteColor]];
            //[myAlertView addSubview:myTextField];
            [myAlertView addSubview:picker];
            
            [myAlertView show]; */

        }

        
        if(indexPath.row == 9){
            
            pickerCount = 1;
            picker.alpha = 1;
                        tableControl2 = 1;
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"TC2"];
            
            cancel.alpha = 1;
            web.alpha = 1;
            elegir.alpha = 1;

            [picker reloadAllComponents];
        }

    }
     }
/////AlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

 if(buttonIndex == 1) {
    
    //    NSLog(@"string entered=%@",myTextField.text);
    
    NSLog(@"string entered=%@",[alertView textFieldAtIndex:0].text);
    
    NSString *string = [alertView textFieldAtIndex:0].text;
    
    if (seleCount == 0) {
        //[poliza setTitle:myTextField.text forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults]setObject:string forKey:@"noPoliz"];
        [self.tableView reloadData];
        [self.tableView reloadInputViews];
    }
    if (seleCount == 1) {
        //  [marca setTitle:myTextField.text forState:UIControlStateNormal];
        
        [[NSUserDefaults standardUserDefaults]setObject:string forKey:@"modelo"];
        [self.tableView reloadData];
        [self.tableView reloadInputViews];
    }
    
    
}



}
//- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 1) {
//        
//    //    NSLog(@"string entered=%@",myTextField.text);
//        
//        NSLog(@"string entered=%@",[alertView textFieldAtIndex:0].text);
//        
//        NSString *string = [alertView textFieldAtIndex:0].text;
//        
//        if (seleCount == 0) {
//            //[poliza setTitle:myTextField.text forState:UIControlStateNormal];
//            [[NSUserDefaults standardUserDefaults]setObject:myTextField.text forKey:@"noPoliz"];
//            [self.tableView reloadData];
//            [self.tableView reloadInputViews];
//        }
//        if (seleCount == 1) {
//          //  [marca setTitle:myTextField.text forState:UIControlStateNormal];
//    
//            [[NSUserDefaults standardUserDefaults]setObject:myTextField.text forKey:@"modelo"];
//            [self.tableView reloadData];
//            [self.tableView reloadInputViews];
//        }
//        
//    
//}
//}

- (void)willPresentAlertView:(UIAlertView *)alertView {
   // [alertView setFrame:CGRectMake(500, 400, 300, 200)];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"timeStamp"] description];
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	
    if(pickerCount == 0){
        return [self.brandArray count];}
    if(pickerCount == 1){
        return [self.seguroArray count];
    }else{
    return [self.yearArray count];
    }
}
#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerCount == 0){
        return [self.brandArray objectAtIndex:row];
    }
    if(pickerCount == 1){
        return [self.seguroArray objectAtIndex:row];
    }else{
    return [self.yearArray objectAtIndex:row];
    }
}



@end
