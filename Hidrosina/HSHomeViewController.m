//
//  HSHomeViewController.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-27.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import "HSHomeViewController.h"
#import "HSBannerCollectionViewCell.h"
#import "hidroMapas.h"
#import "HSBannerDetailVC.h"

#import "Servicios.h"
#import "Facturacion.h"

#import "WebController.h"
#import "HSWebViewController.h"

#define kNavBarHeight 100.0f


@interface HSHomeViewController ()

@property (nonatomic) IBOutlet UICollectionView * bannerCollectionView;
@property (nonatomic) IBOutlet UIButton *estaciones;
@property (nonatomic) IBOutlet UIButton *servicios;

@property (nonatomic, retain) NSArray * banners;
@property (nonatomic, retain) NSArray * bannersFull;

@end

@implementation HSHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, kNavBarHeight-1, 320, 500))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    NSArray *array =[[NSArray alloc]initWithObjects:@"1.png",@"2.png",@"3.png",@"4.png",@"5.png", nil];
    self.banners = array;
    
    NSArray *arrayF =[[NSArray alloc]initWithObjects:@"1F.png",@"2F.png",@"3F.png",@"4F.png",@"5F.png", nil];
    self.bannersFull = arrayF;
    
    [_bannerCollectionView setDataSource:self];
    [_bannerCollectionView setDelegate:self];
    [_bannerCollectionView setBackgroundColor:[UIColor clearColor]];
    
    for (int i =0 ; i<[array count]; i++) {
    [self registerCellsWithIDs:i ];
    }
}

- (void)viewWillAppear:(BOOL)animated{

    self.navigationController.navigationBar.hidden=YES;
    
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden=NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)awakeFromNib {
    
//    SWWidgetQuickAccessLayout *flowLayout=[[SWWidgetQuickAccessLayout alloc] init];
//    CGFloat wWidth = [[UIScreen mainScreen] applicationFrame].size.width;
//    self.collectionView.frame = CGRectMake(0, 0, wWidth, self.frame.size.height);
//    
//    [_collectionView setCollectionViewLayout:flowLayout];

}

- (void)registerCellsWithIDs: (int)iD {
    
    ///Register Nib to introduce in the cells. Later we will pass an array full of xibs, each one corresponding to a specific widget.
    UINib * themeCellNib = [UINib nibWithNibName:@"HSBannerCollectionViewCell" bundle:nil];
    [_bannerCollectionView registerNib:themeCellNib forCellWithReuseIdentifier:[NSString stringWithFormat:@"Element%i",iD ]];
    
}

#pragma mark - UICollectionView Flow Layout Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    CGFloat wWidth = 270.0f;
//    CGFloat wHeight = 180.0f;

    CGFloat wWidth = self.bannerCollectionView.frame.size.width - 20;
    CGFloat wHeight = self.bannerCollectionView.frame.size.height;
    
    CGSize widgetSize = CGSizeMake(wWidth , wHeight);
    
    return widgetSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.banners count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HSBannerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NSString stringWithFormat:@"Element%li",(long)indexPath.row ] forIndexPath:indexPath];
 //   [cell setImage:indexPath.row];
   
        [cell downloadImage:[self.banners objectAtIndex:indexPath.row]];
    
    
  //  [cell update:[self.bannerCollectionView objectAtIndex:indexPath.item]];
    
    NSLog(@"%li, %@", (long)indexPath.row, [self.banners objectAtIndex:indexPath.row]);
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    HSBannerDetailVC *bannerDetail = [[HSBannerDetailVC alloc]initWithImage:[self.bannersFull objectAtIndex:indexPath.row] andURL:[NSString stringWithFormat:@"%ld + 1", (long)indexPath.row]];
    
    bannerDetail.urlString = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    [self.navigationController presentViewController:bannerDetail animated:YES completion:nil];

}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0){
    
 //   cell= nil;
    
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(IBAction)pushMaps
{
    hidroMapas *mp = [[hidroMapas alloc]initWithNibName:@"hidroMapas" bundle:[NSBundle mainBundle]];
    
   // self.hMaps = mp;
    [self.navigationController pushViewController:mp animated:YES];
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"header2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    
    
}

-(IBAction)pushServ{
    
    Facturacion *mp = [[Facturacion alloc]initWithNibName:@"Facturacion" bundle:[NSBundle mainBundle]];
    
 //   self.fact = mp;
    [self.navigationController pushViewController:mp animated:YES];
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"header2.png"] drawInRect:self.navigationController.navigationBar.bounds];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    
    
    
}

-(IBAction)openFaceBookPage{ //235920859903673
    
    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/235920859903673"];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://facebook.com"]];
    }

}

-(IBAction)pushWebSite{
    
    HSWebViewController *bannerDetail = [[HSWebViewController alloc]init];
    [bannerDetail setUrl:@" http://www.hidrosina.com.mx"];
    
    [self.navigationController presentViewController:bannerDetail animated:YES completion:nil];
    
}

- (IBAction)openWebSite:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.hidrosina.com.mx/hidrosina/Wf_Pag_principal.aspx"]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
