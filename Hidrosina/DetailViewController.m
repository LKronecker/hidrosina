//
//  DetailViewController.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "DetailViewController.h"
#import "HSPlaces.h"

@interface DetailViewController ()
@property (nonatomic) HSPlaces * places;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *userLocation;
- (void)configureView;
@end

@implementation DetailViewController

@synthesize mapView, latitudeArray, longitudeArray, tittleArray, subtArray, gdet, control, gasArray, RangeControl, km1Button, km2Button, km3Button, home, webView, activityindicator, webView1, tabbar, HIDROwebView, scrollView, image0, image1, image2, image3, image4, imageContainer, imageDet, timer, mapTab, km4Button;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
    }
}

-(IBAction)changeSeg:(id)sender{
	
	if (control.selectedSegmentIndex == 0) {
		mapView.mapType = MKMapTypeStandard;
	}
	if (control.selectedSegmentIndex == 1) {
		mapView.mapType = MKMapTypeSatellite;
	}
	if (control.selectedSegmentIndex == 2) {
		mapView.mapType = MKMapTypeHybrid;
	}
    
}

-(IBAction)changeRange:(id)sender{
    
    if (control.selectedSegmentIndex == 0) {
		[self gotoLocation];
	}
	if (control.selectedSegmentIndex == 1) {
		[self gotoLocation1];
	}
	if (control.selectedSegmentIndex == 2) {
		[self gotoLocation2];
	}
    
    
}

- (void)downloadImage:(NSString *)image inBackground:(UIImageView *)imageView
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(imageView.bounds.size.width/2, imageView.bounds.size.height/2)];
    [imageView addSubview:activity];
    [activity startAnimating];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://i1065.photobucket.com/albums/u395/lkronecker/%@",image]];
    
    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
    dispatch_async(download_queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *_image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            [activity removeFromSuperview];
            [imageView setImage:_image];
        });
    });
    // dispatch_release(download_queue);
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
  
       
     [tabbar setSelectedItem:[tabbar.items objectAtIndex:0]];
    
    bannerCount =7;
    ///
    imageContainer.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap2:)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageContainer addGestureRecognizer:recognizer];
    
    imageContainer.alpha = 0;


    ///


    HIDROwebView.alpha = 0;

    
    tabbar.delegate =self;
    self.webView.delegate = self;

    [self.view bringSubviewToFront:tabbar];
    //[self.view bringSubviewToFront:km2Button];
    //[self.view bringSubviewToFront:km3Button];
    
    //   [webView sendSubviewToBack:km1Button];
    ///////
    km2Button.selected = YES;
    

    [NSTimer scheduledTimerWithTimeInterval:4.5 target:self selector:@selector(places:) userInfo:nil repeats:NO];
    ///////////////
    
    ////// Scroll view
//    CGRect scrollViewFrame = CGRectMake(270, 700, 395, 180);
//    self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
//    
//    [self.view addSubview: self.scrollView];
//    CGSize scrollViewContentSize = CGSizeMake(600, 600);
//    [self.scrollView setContentSize:scrollViewContentSize];
//    scrollView.delegate = self;
//    
//    [self.scrollView setBackgroundColor:[UIColor blackColor]];
//    [scrollView setCanCancelContentTouches:NO];
//    
//    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
//    scrollView.clipsToBounds = YES;
//    scrollView.scrollEnabled = YES;
//    scrollView.pagingEnabled = YES;
//    scrollView.alpha = 1;
//    scrollView.backgroundColor = nil;
//    
//    NSUInteger nimages = 0;
//    CGFloat cx = 0;
//    for (; ; nimages++) {
//        NSString *imageName = [NSString stringWithFormat:@"%d.png", (nimages)];
//        //  NSString *imageName = [NSString stringWithFormat:@"cuadro-de-textoN.png"];
//        UIImage *image = [UIImage imageNamed:imageName];
//        
//        if (image == nil) {
//            break;
//        }
//        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
//        
//        CGRect rect = imageView.frame;
//        rect.size.height = 180;
//        rect.size.width = 297;
//        rect.origin.x = ((scrollView.frame.size.width - (image.size.width))/2) + cx;
//        rect.origin.y = ((scrollView.frame.size.height - image.size.height)/2);
//        
//        imageView.frame = rect;
//        
//        [scrollView addSubview:imageView];
//        // [imageView release];
//        
//        cx += scrollView.frame.size.width;
//        
//        
//        
//        if ([imageName isEqualToString:@"0.png"]) { imageView.userInteractionEnabled = YES;
//            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap0:)];
//            recognizer.numberOfTapsRequired = 1;
//            recognizer.numberOfTouchesRequired = 1;
//            self.image0 = imageView;
//            
//            [imageView addGestureRecognizer:recognizer];}
//        if ([imageName isEqualToString:@"1.png"]) { imageView.userInteractionEnabled = YES;
//            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap1:)];
//            recognizer.numberOfTapsRequired = 1;
//            recognizer.numberOfTouchesRequired = 1;
//            self.image1 = imageView;
//            
//            [imageView addGestureRecognizer:recognizer];}
//        if ([imageName isEqualToString:@"2.png"]) { imageView.userInteractionEnabled = YES;
//            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap2A:)];
//            recognizer.numberOfTapsRequired = 1;
//            recognizer.numberOfTouchesRequired = 1;
//            self.image2 = imageView;
//            
//            [imageView addGestureRecognizer:recognizer];}
//        if ([imageName isEqualToString:@"3.png"]) { imageView.userInteractionEnabled = YES;
//            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap3:)];
//            recognizer.numberOfTapsRequired = 1;
//            recognizer.numberOfTouchesRequired = 1;
//            self.image3 = imageView;
//            
//            [imageView addGestureRecognizer:recognizer];}
//        if ([imageName isEqualToString:@"4.png"]) { imageView.userInteractionEnabled = YES;
//            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap4:)];
//            recognizer.numberOfTapsRequired = 1;
//            recognizer.numberOfTouchesRequired = 1;
//            
//            self.image4 = imageView;
//            
//            [imageView addGestureRecognizer:recognizer];}
//        
//        
//        
//    }
//    
//    
//    [scrollView setContentSize:CGSizeMake(cx, [scrollView bounds].size.height)];
//    
//    
//    timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//
///////
//    [self downloadImage:@"0.png" inBackground:image0];
//    [self downloadImage:@"1.png" inBackground:image1];
//    [self downloadImage:@"2.png" inBackground:image2];
//    [self downloadImage:@"3.png" inBackground:image3];
//    [self downloadImage:@"4.png" inBackground:image4];

	// Do any additional setup after loading the view, typically from a nib.
    [self initiLocationManager];
    [self setupMap];
    [self configureView];
    
    [super viewDidLoad];
}

- (void) setupMap {
    
    mapView = [[MKMapView alloc]initWithFrame:self.view.bounds];
    [self.view insertSubview:mapView atIndex:0];
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    
    [mapView setDelegate:self];
    
    mapView.showsUserLocation = YES;


}

-(void)initiLocationManager {

    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [_locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *userLoc = [locations firstObject];
    self.userLocation = userLoc;

}


-(void)loading{
//    if(!webView.loading){
//        [activityindicator stopAnimating];
//        activityindicator.hidden = YES;}
//	
//    else{
//		[activityindicator startAnimating];
//        activityindicator.hidden = NO; }
}


-(IBAction)places:(id)sender{
    
    
    self.places = [[HSPlaces alloc]init];
    
    self.tittleArray = [self.places tittles];
    self.gasArray = [self.places gasStations];
    self.latitudeArray = [self.places latitudes];
    self.longitudeArray = [self.places longitudes];
    self.subtArray = [self.places subtittles];
    
    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    //  NSString *GPSstring = [[NSString alloc]initWithFormat:@"GPS: %f, %f", userCoordinate.latitude, userCoordinate.longitude];
    //GPSlabel.text = GPSstring;
	
	NSLog(@"user latitude = %f",userCoordinate.latitude);
	NSLog(@"user longitude = %f",userCoordinate.longitude);
	
	mapView.delegate=self;
    
	NSMutableArray* annotations=[[NSMutableArray alloc] init];
    
    for (int i=0; i<[self.tittleArray count]; i++) {
        
        double lon = [[longitudeArray objectAtIndex:i] doubleValue];
        double lat = [[latitudeArray objectAtIndex:i] doubleValue];
        
        NSString *tittleString = [[NSString alloc]initWithFormat:@"%@", [tittleArray objectAtIndex:i]];
        NSString *subttlString = [[NSString alloc]initWithFormat:@"%@",[subtArray objectAtIndex:i]];
        
        //:@"%f, %f",[[latitudeArray objectAtIndex:i] doubleValue], [[longitudeArray objectAtIndex:i] doubleValue]];
        
        CLLocationCoordinate2D theCoordinate;
        theCoordinate.latitude = lat;
        theCoordinate.longitude = lon;
        
        MapAnotations* myAnnotation1=[[MapAnotations alloc] init];
        
        myAnnotation1.coordinate=theCoordinate;
        myAnnotation1.title = tittleString;
        myAnnotation1.subtitle = subttlString;
        //  myAnnotation1.pinColor = MKPinAnnotationColorPurple;
        
        [mapView addAnnotation:myAnnotation1];
        [annotations addObject:myAnnotation1];
        
    }
    
    [self gotoLocation1];
    
    
    
}

- (void)gotoLocation
{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.01f;
    newRegion.span.longitudeDelta = 0.01f;
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
}
- (void)gotoLocation1 {
    
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.05f;
    newRegion.span.longitudeDelta = 0.05;
	
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
}
- (void)gotoLocation2 {
    
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.1f;
    newRegion.span.longitudeDelta = 0.1f;
	
    [self.mapView setRegion:newRegion animated:YES];
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
}

-(IBAction)goLoc:(id)sender{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.01f;
    newRegion.span.longitudeDelta = 0.01f;
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    km1Button.selected = YES;
    km2Button.selected = NO;
    km3Button.selected = NO;
    km4Button.selected = NO;
}
-(IBAction)goLoc1:(id)sender{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.05f;
    newRegion.span.longitudeDelta = 0.05;
	
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    km1Button.selected = NO;
    km2Button.selected = YES;
    km3Button.selected = NO;
        km4Button.selected = NO;
    
}
-(IBAction)goLoc2:(id)sender{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.1f;
    newRegion.span.longitudeDelta = 0.1f;
	
    [self.mapView setRegion:newRegion animated:YES];
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    km1Button.selected = NO;
    km2Button.selected = NO;
    km3Button.selected = YES;
        km4Button.selected = NO;
    
    
}
-(IBAction)goLoc3:(id)sender{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.2f;
    newRegion.span.longitudeDelta = 0.2f;
	
    [self.mapView setRegion:newRegion animated:YES];
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    km1Button.selected = NO;
    km2Button.selected = NO;
    km3Button.selected = NO;
        km4Button.selected = YES;
    
    
}


#pragma mark MKMapViewDelegate
/*
 - (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
 {
 return [kml viewForOverlay:overlay];
 }
 */
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	//NSLog(@"welcome into the map view annotation");
	
	// if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	// try to dequeue an existing pin view first
	static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
	MKPinAnnotationView* pinView = [[MKPinAnnotationView alloc]
                                    initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
    
    pinView.image = [UIImage imageNamed:@"pin.png"];
    pinView.animatesDrop=YES;
	pinView.canShowCallout=YES;
    pinView.pinColor=MKPinAnnotationColorGreen;
    
    
    
	
	
	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	[rightButton setTitle:annotation.title forState:UIControlStateNormal];
	[rightButton addTarget:self
					action:@selector(showDetails:)
		  forControlEvents:UIControlEventTouchUpInside];
	pinView.rightCalloutAccessoryView = rightButton;
	
	UIImageView *profileIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin-1.png"]];
	pinView.leftCalloutAccessoryView = profileIconView;
	//[profileIconView release];
	
	
	return pinView;
}

-(IBAction)pushDemo{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"DEMO Desarrollado por ELO" message:@"Esta opción aun no esta activada." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
    // [alert release];
    
}


-(IBAction)showDetails:(id)sender{
    
	//NSLog(@"Annotation Click");
    
    //To be safe, may want to check that array has at least one item first.
    
    //   id<MKAnnotation> ann = [[mapView selectedAnnotations] objectAtIndex:0];
    
    // OR if you have custom annotation class with other properties...
    // (in this case may also want to check class of object first)
    
    MapAnotations *an = [[mapView selectedAnnotations] objectAtIndex:0];
    
    NSLog(@"ann.title = %@", an.title);
    //  NSLog(@"ann.coord = %@", an.coordinate);
    
    ////////
    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    
    NSString *imST = [[NSString alloc]initWithFormat:@"%@",an.title];
    int index = [tittleArray indexOfObject:imST];
    
    GasDetails *det = [[GasDetails alloc]initWithNibName:@"GasDetails_iPad" bundle:[NSBundle mainBundle]];
    
    
    NSString *laST = [NSString stringWithFormat:@"%f", userCoordinate.latitude];
    NSString *loST = [NSString stringWithFormat:@"%f", userCoordinate.longitude];
    NSString *gST = [NSString stringWithFormat:@"%@", [gasArray objectAtIndex:index]];
    
    det.longST = loST;
    det.latST = laST;
    
    det.gasST = gST;
    
    NSLog(@"%@", gST);
    
    det.diesel.hidden = YES;
    
    
    
    
    det.titl = an.title;
    
    NSString *contSt = [[NSString alloc]initWithFormat:@"%i",index];
    det.detCount = contSt;
    NSLog(@"%i, %@",index, contSt);
    
    
    self.gdet = det;
    [self.navigationController pushViewController:det animated:YES];
}

-(void)webView:(UIWebView *)webview didFailLoadWithError:(NSError *)error {
    
    // if (error.code == NSURLErrorNotConnectedToInternet){
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Imposible descargar contenido, verifique su conexión a Internet."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    NSLog(@"Alert");
    
    webView.alpha = 0;
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"", @"");
    }
    return self;
}

- (void)handleTap2:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        // [self.view clearsContextBeforeDrawing];
        // self.navigationController.navigationBarHidden = NO;
        
        [UIView beginAnimations:@"Dilation" context:nil];
        [UIView setAnimationDuration:1.0];
        imageContainer.alpha = 0;
        CGRect newFrame = CGRectMake(391, 814, 171 , 76);
        
     //   (171, 76, 391 , 814)
        
        // newFrame.origin.y = 146;
        //newFrame.origin.x = 361;
        
        imageContainer.frame = newFrame;
        
    //    estaciones2.alpha = 1;
      //  servicios2.alpha = 1;
        self.scrollView. alpha = 1;
        
        [UIView commitAnimations];
        
        [self.imageDet removeFromSuperview];
        // [self.view removeFromSuperview];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:3.5 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
        //  NSLog(@"WEY!!!");
    }
}

							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Vehículo", @"Vehículo");
    //self.splitViewController.barButtonItem.frame
//    barButtonItem.image = [UIImage imageNamed:@"Mi-vehículo.png"];
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:NO];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

#pragma mark UITabBarDelegate

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    // NSLog(@"rawr");
    if (item.tag == 0) {
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        mapView.alpha=1;
        RangeControl.alpha = 1;
        km1Button.alpha = 1;
        km2Button.alpha = 1;
        km3Button.alpha = 1;
         km4Button.alpha = 1;
        webView.alpha = 0.6;
                 HIDROwebView.alpha = 0;
        [UIView commitAnimations];
        [self.view bringSubviewToFront:km1Button];
         [self.view bringSubviewToFront:tabbar];
    }
    if (item.tag == 1) {
        NSString *HidroUrlAddress = @"http://www.hidrosina.com.mx/hidrosina2/WF_login.aspx";
        NSURL *Hurl = [NSURL URLWithString:HidroUrlAddress];
        NSURLRequest *HrequestObj = [NSURLRequest requestWithURL:Hurl];
        //  HIDROwebView = [[UIWebView alloc]initWithFrame:self.view.bounds];
        
        [HIDROwebView loadRequest:HrequestObj];
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        mapView.alpha=0;
        RangeControl.alpha = 0;
        km1Button.alpha = 0;
        km2Button.alpha = 0;
        km3Button.alpha = 0;
        km4Button.alpha = 0;
      //  webView.alpha = 0;
         HIDROwebView.alpha = 1;
       [ UIView commitAnimations];
        
        
        
      
        [self.view addSubview:webView];
        
        [self.view bringSubviewToFront:tabbar];

    }
    if (item.tag == 2) {
        NSString *HidroUrlAddress = @"http://arcadia.hidrosina.com.mx/index.html";
        NSURL *Hurl = [NSURL URLWithString:HidroUrlAddress];
        NSURLRequest *HrequestObj = [NSURLRequest requestWithURL:Hurl];
        //  HIDROwebView = [[UIWebView alloc]initWithFrame:self.view.bounds];
        
        [HIDROwebView loadRequest:HrequestObj];
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        mapView.alpha=0;
        RangeControl.alpha = 0;
        km1Button.alpha = 0;
        km2Button.alpha = 0;
        km3Button.alpha = 0;
                km4Button.alpha = 0;
     //   webView.alpha = 0;
        HIDROwebView.alpha = 1;
        [ UIView commitAnimations];
        
        [self.view bringSubviewToFront:tabbar];
        

    }
    if (item.tag == 3) {
        NSString *HidroUrlAddress = @"http://facturacion.hidrosina.com.mx/hidrosina/wf_loginfe.aspx";
        NSURL *Hurl = [NSURL URLWithString:HidroUrlAddress];
        NSURLRequest *HrequestObj = [NSURLRequest requestWithURL:Hurl];
        //  HIDROwebView = [[UIWebView alloc]initWithFrame:self.view.bounds];
        
        [HIDROwebView loadRequest:HrequestObj];
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        mapView.alpha=0;
        RangeControl.alpha = 0;
        km1Button.alpha = 0;
        km2Button.alpha = 0;
        km3Button.alpha = 0;
                km4Button.alpha = 0;
    //    webView.alpha = 0;
        HIDROwebView.alpha = 1;
        [ UIView commitAnimations];
        
        
        [self.view bringSubviewToFront:tabbar];
    }
    if (item.tag == 4) {
        NSString *HidroUrlAddress = @"http://www.hidrosina.com.mx/new/mobile/contacto.html";
        NSURL *Hurl = [NSURL URLWithString:HidroUrlAddress];
        NSURLRequest *HrequestObj = [NSURLRequest requestWithURL:Hurl];
        //  HIDROwebView = [[UIWebView alloc]initWithFrame:self.view.bounds];
        
        [HIDROwebView loadRequest:HrequestObj];
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        mapView.alpha=0;
        RangeControl.alpha = 0;
        km1Button.alpha = 0;
        km2Button.alpha = 0;
        km3Button.alpha = 0;
                km4Button.alpha = 0;
      //  webView.alpha = 0;
        HIDROwebView.alpha = 1;
        [ UIView commitAnimations];
        
        
        [self.view bringSubviewToFront:tabbar];
        
    }
    if (item.tag == 5) {
        NSString *HidroUrlAddress = @"http://www.hidrosina.com.mx/new/mobile/nosotros.html";
        NSURL *Hurl = [NSURL URLWithString:HidroUrlAddress];
        NSURLRequest *HrequestObj = [NSURLRequest requestWithURL:Hurl];
        //  HIDROwebView = [[UIWebView alloc]initWithFrame:self.view.bounds];
        
        [HIDROwebView loadRequest:HrequestObj];
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        mapView.alpha=0;
        RangeControl.alpha = 0;
        km1Button.alpha = 0;
        km2Button.alpha = 0;
        km3Button.alpha = 0;
      //  webView.alpha = 0;
        HIDROwebView.alpha = 1;
        [ UIView commitAnimations];
        
        
        [self.view bringSubviewToFront:tabbar];
        
    }
    


}

- (void)tabBarController:(UITabBarController *)tabBarController willBeginCustomizingViewControllers:(NSArray *)viewControllers
{
    
}

- (void)handleTap0:(UITapGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        if(timer)
        {
            [timer invalidate];
            timer = nil;
        }
        
        
        [self.imageDet removeFromSuperview];
        
        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        self.scrollView. alpha = 0;
        
        [UIView beginAnimations:@"Dilation" context:nil];
        [UIView setAnimationDuration:0.5];
        imageContainer.alpha = 1;
        CGRect newFrame = self.view.frame;
        newFrame.origin.y = 0;
        newFrame.origin.x = 0;
        
        imageContainer.frame = newFrame;
        
        self.scrollView. alpha = 0;
     //   estaciones2.alpha = 0;
       // servicios2.alpha = 0;
        
        
        [UIView commitAnimations];
        
        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        //  view.alpha = 0.7;
        
        
       // [self.navigationController.navigationBar addSubview:view];
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails) userInfo:nil repeats:NO];
        
        //  [scrollView removeFromSuperview];
        
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
        
        [self downloadImage:@"0F_iPad.png" inBackground:imageView];
        self.imageDet = imageView;
        
        [self.imageContainer addSubview:imageDet];
        
        
        
    }
}
- (void)handleTap1:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        if(timer)
        {
            [timer invalidate];
            timer = nil;
        }
        
        
        [self.imageDet removeFromSuperview];
        
        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        
        [UIView beginAnimations:@"Dilation" context:nil];
        [UIView setAnimationDuration:0.5];
        imageContainer.alpha = 1;
        CGRect newFrame = self.view.frame;
        newFrame.origin.y = 0;
        newFrame.origin.x = 0;
        
        imageContainer.frame = newFrame;
        self.scrollView. alpha = 0;
       // estaciones2.alpha = 0;
        //servicios2.alpha = 0;
        
        [UIView commitAnimations];
        
        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        //  view.alpha = 0.7;
        
        
      //  [self.navigationController.navigationBar addSubview:view];
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails1) userInfo:nil repeats:NO];
        
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
        
        [self downloadImage:@"1F_iPad.png" inBackground:imageView];
        self.imageDet = imageView;
        
        [self.imageContainer addSubview:imageDet];
    }
}
- (void)handleTap2A:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self.imageDet removeFromSuperview];
        
        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        
        [UIView beginAnimations:@"Dilation" context:nil];
        [UIView setAnimationDuration:0.5];
        imageContainer.alpha = 1;
        CGRect newFrame = self.view.frame;
        newFrame.origin.y = 0;
        newFrame.origin.x = 0;
        
        imageContainer.frame = newFrame;
     //   estaciones2.alpha = 0;
      //  servicios2.alpha = 0;
        self.scrollView. alpha = 0;
        
        [UIView commitAnimations];
        
        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        //  view.alpha = 0.7;
        
       // [self.navigationController.navigationBar addSubview:view];
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails2) userInfo:nil repeats:NO];
        
        if(timer)
        {
            [timer invalidate];
            timer = nil;
        }
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
        
        [self downloadImage:@"2F_iPad.png" inBackground:imageView];
        self.imageDet = imageView;
        
        [self.imageContainer addSubview:imageDet];
        
    }
}
- (void)handleTap3:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self.imageDet removeFromSuperview];
        
        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        
        [UIView beginAnimations:@"Dilation" context:nil];
        [UIView setAnimationDuration:0.5];
        imageContainer.alpha = 1;
        CGRect newFrame = self.view.frame;
        newFrame.origin.y = 0;
        newFrame.origin.x = 0;
        
        imageContainer.frame = newFrame;
        self.scrollView. alpha = 0;
        
     //   estaciones2.alpha = 0;
       // servicios2.alpha = 0;
        
        [UIView commitAnimations];
        
        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        //  view.alpha = 0.7;
        
      //  [self.navigationController.navigationBar addSubview:view];
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails3) userInfo:nil repeats:NO];
        
        if(timer)
        {
            [timer invalidate];
            timer = nil;
        }
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
        
        [self downloadImage:@"3F_iPad.png" inBackground:imageView];
        self.imageDet = imageView;
        
        [self.imageContainer addSubview:imageDet];
        
    }
}
- (void)handleTap4:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        [self.imageDet removeFromSuperview];
        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        
        [UIView beginAnimations:@"Dilation" context:nil];
        [UIView setAnimationDuration:0.5];
        imageContainer.alpha = 1;
        CGRect newFrame = self.view.frame;
        newFrame.origin.y = 0;
        newFrame.origin.x = 0;
        
        imageContainer.frame = newFrame;
        self.scrollView. alpha = 0;
        
      //  estaciones2.alpha = 0;
      //  servicios2.alpha = 0;
        
        [UIView commitAnimations];
        
        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
        //  view.alpha = 0.7;
        
    //    [self.navigationController.navigationBar addSubview:view];
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails4) userInfo:nil repeats:NO];
        
        if(timer)
        {
            [timer invalidate];
            timer = nil;
        }
        
        
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
        
        [self downloadImage:@"4F_iPad.png" inBackground:imageView];
        self.imageDet = imageView;
        
        [self.imageContainer addSubview:imageDet];
        
    }
}

-(IBAction)slide:(id)sender{
    
    if (bannerCount > 1400) {
        bannerCount = 0;
        
        CGPoint bottomOffset = CGPointMake(0, bannerCount);
        [self.scrollView setContentOffset:bottomOffset animated:NO];
    }else{
        
        bannerCount += 392;
        
        CGPoint bottomOffset = CGPointMake(bannerCount, 0);
        [self.scrollView setContentOffset:bottomOffset animated:YES];}
    
    //if (bannerCount == 1) bannerCount = 7;
    
    NSLog(@"sirve");
}



#pragma mark -
#pragma mark Scroll View Delegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollV
{
    
    
    //  lastScrollPosition->x = scrollV.contentOffset.x / 55;
    //  NSLog(@"damn");
    //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
    if(timer)
    {
        [timer invalidate];
        timer = nil;
    }
    
    //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    timer = [NSTimer scheduledTimerWithTimeInterval:4.5 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    //   timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
    /*   ScrollDirection scrollDirection;
     if (self.lastContentOffset > scrollView.contentOffset.x){
     scrollDirection = ScrollDirectionRight;
     //  bannerCount -= 392;
     //  NSLog(@"Right");
     }
     else if (self.lastContentOffset < scrollView.contentOffset.x){
     scrollDirection = ScrollDirectionLeft;
     //    bannerCount += 392;
     //  NSLog(@"Left, %i", bannerCount);
     }
     self.lastContentOffset = scrollView.contentOffset.x;
     
     // do whatever you need to with scrollDirection here. */
}

-(void) imageDetails{
    
    //////
    
    //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
    
    
    //
}
-(void) imageDetails1{
    
    //    timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
    
    //
}
-(void) imageDetails2{
    
    //   timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
}
-(void) imageDetails3{
    //timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
}
-(void) imageDetails4{
    //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    
}


-(BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return YES;
}

///////////////////
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (orientationchangefunction:) name:UIDeviceOrientationDidChangeNotification object:nil];
    [self orientationchngfn];
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        orientseason=0;
    }
    if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        orientseason=1;
    }
    if(interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        orientseason=1;
    }
    if(interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        orientseason=0;
    }
    if(orientseason==0)
    {
    }
    else if(orientseason==1)
    {
        
    }
    
    return YES;
}

-(void) orientationchangefunction:(NSNotification *) notificationobj
{
    
    [self performSelector:@selector(orientationchngfn) withObject:nil afterDelay:0.0];
}
-(void) orientationchngfn
{
    UIDeviceOrientation dorientation =[UIDevice currentDevice].orientation;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        if(UIDeviceOrientationIsPortrait(dorientation))
            
        {
            orientseason=0;
        }
        else if (UIDeviceOrientationIsLandscape(dorientation))
        {
            orientseason=1;
        }
        if(orientseason==0)
        {
            mapView.frame=CGRectMake(0, 0, 768, 1004);
            
            
        }
        else if(orientseason==1)
        {
           mapView.frame=CGRectMake(0, 0, 1004, 768);
            
        }
        
    }
    else {
        if(UIDeviceOrientationIsPortrait(dorientation))
            
        {
            orientseason=0;
        }
        else if (UIDeviceOrientationIsLandscape(dorientation))
        {
            
            orientseason=1;
        }
        if(orientseason==0)
        {
            mapView.frame=CGRectMake(0, 0, 768, 1004);
            
            scrollView.frame = CGRectMake(270, 700, 395, 180);
            
            UIGraphicsBeginImageContext (CGSizeMake(768, 90));
            [[UIImage imageNamed:@"header-2.png"] drawInRect:(CGRectMake(0, 20, 768, 45))];
            UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UILabel *paddingLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, -20, self.view.bounds.size.width, 20)];
            paddingLabel.backgroundColor = [UIColor greenColor];
            [self.navigationController.navigationBar addSubview:paddingLabel];
            
            [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
            
           
              self.scrollView.alpha = 1;
          //  km1Button.frame = CGRectMake(0, 925, 256, 30);
            //            km2Button.frame = CGRectMake(256, 925, 256, 30);
              //          km3Button.frame = CGRectMake(512, 925, 256, 30);
            
            
        }
        else if(orientseason==1)
        {
            
           
            mapView.frame=CGRectMake(0, 0, 1034, 768);
            
            scrollView.frame = CGRectMake(700, 475, 395, 250);
            
            UIGraphicsBeginImageContext (CGSizeMake(1024, 90));
            [[UIImage imageNamed:@"headerG.png"] drawInRect:(CGRectMake(0, 20, 1024, 45))];
            UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UILabel *paddingLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, -20, self.view.bounds.size.width, 20)];
            paddingLabel.backgroundColor = [UIColor greenColor];
            [self.navigationController.navigationBar addSubview:paddingLabel];
            
            [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
            
            
            km1Button.frame = CGRectMake(0, 500, 335, 30);
            km2Button.frame = CGRectMake(335, 738, 335, 30);
            km3Button.frame = CGRectMake(670, 738, 335, 30);
             self.scrollView.alpha = 1;
            NSLog(@"TURN!!!");

            
        }
    }
    
}



@end
