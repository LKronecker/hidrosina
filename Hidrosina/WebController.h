//
//  WebController.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-04-22.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebController : UIViewController<UIWebViewDelegate>{
    UIWebView *webView;
    NSTimer *timer;
    UIActivityIndicatorView *activityindicator;
    
    
    
    
}
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet     UIActivityIndicatorView *activityindicator;

@property (nonatomic, retain)  NSString *urlString;



@end
