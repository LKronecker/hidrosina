//
//  HSBannerCollectionViewCell.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-27.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSBannerCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *bannerImageView;
@property (nonatomic) NSMutableArray *bannerImagesArray;
@property (nonatomic) UIImage *bannerImage;

- (void)downloadImage:(NSString *)image;
-(void)setImage:(NSInteger)imageNum;

@end
