//
//  Servicios.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "Servicios.h"
#define IS_WIDESCREEN ( fabs (( double ) [[ UIScreen mainScreen ] bounds].size.height - (double )568 ) < DBL_EPSILON)
@interface Servicios ()

@end

@implementation Servicios

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    if (IS_WIDESCREEN) {
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageNamed:@"fondo-servicios2.png"] drawInRect:(CGRectMake(0, 0, 320, 500))];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];

    }else{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-servicios2.png"] drawInRect:(CGRectMake(0, 0, 320, 420))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    }
////////////////
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 25);
    [button setTitle:@"Back" forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)gotoBack:(id)sender
{
    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"nab-bar2.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    //////////
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, -62, 320, 480))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ///////
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)pushDemo{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"DEMO Desarrollado por ELO" message:@"Esta opción aun no esta activada." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
    // [alert release];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
