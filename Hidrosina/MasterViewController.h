//
//  MasterViewController.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate>{
    
    UIButton *elegir;
    UIButton *cancel;
    UIButton *web;
    NSArray *autoArray;
    
    int pickerCount;
    bool tableControl;
    bool tableControl1;
    bool tableControl2;
    bool tableControl3;
    bool tableControl4;
    bool seleCount;
    UIPickerView *picker;
    
    NSArray *brandArray;
    NSArray *seguroArray;
    NSArray *urlSegArray;
    NSArray *urlArray;
    NSArray *numArray;
     NSArray *yearArray;
    
    NSArray *pickerArray;
    
    NSString *marca;
    NSString *seguro;
    NSString *ano;
    UITextField *myTextField;

}
@property (nonatomic, retain) IBOutlet UIButton *elegir;
@property (nonatomic, retain) IBOutlet  UIButton *cancel;
@property (nonatomic, retain) IBOutlet UIButton *web;

 @property (nonatomic, retain)  NSArray *autoArray;
@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain)  NSArray *seguroArray;
@property (nonatomic, retain)  NSArray *urlSegArray;
@property (nonatomic, retain)  NSArray *numArray;
@property (nonatomic, retain) NSArray *urlArray;
@property (nonatomic, retain)  NSArray *brandArray;
  @property (nonatomic, retain)    NSArray *yearArray;
 @property (nonatomic, retain)   UIPickerView *picker;

  @property (nonatomic, retain)  NSArray *pickerArray;
@property (nonatomic, retain) UITextField *myTextField;
@property (nonatomic, retain) NSString *marca;
@property (nonatomic, retain) NSString *seguro;
@property (nonatomic, retain) NSString *ano;

-(IBAction)cancel:(id)sender;
-(IBAction)elegirMarca:(id)sender;
-(IBAction)web:(id)sender;


@end
