//
//  HSWebViewController.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-11-02.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSWebViewController : UIViewController <UIWebViewDelegate>

@property ( retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet     UIActivityIndicatorView *activityindicator;
@property (nonatomic, retain)  NSString *urlString;

- (void)loadWebSitewithURL : (NSString *)url;

- (void)setUrl:(NSString *)urlStr;

@end
