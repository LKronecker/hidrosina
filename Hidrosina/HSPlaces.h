//
//  HSPlaces.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2015-03-15.
//  Copyright (c) 2015 Leopoldo G Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HSPlaces : NSObject

- (NSArray *) tittles;
- (NSArray *) subtittles;
- (NSArray *) gasStations;
- (NSArray *) longitudes;
- (NSArray *) latitudes;

@end
