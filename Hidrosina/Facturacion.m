//
//  Facturacion.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "Facturacion.h"
#define IS_WIDESCREEN ( fabs (( double ) [[ UIScreen mainScreen ] bounds].size.height - (double )568 ) < DBL_EPSILON)

@interface Facturacion ()

@end

@implementation Facturacion
@synthesize emer, info, rendi;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 85, 35);
    //[button setTitle:@"Regresar" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"BOTÓN-REGRESAR.png"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;    /////
    

    ///////
    if(IS_WIDESCREEN){
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self
                   action:@selector(pushInfo:)
         forControlEvents:UIControlEventTouchDown];
       // [button setTitle:@"Show View" forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"Botón-Información-vehicular.png"] forState:UIControlStateNormal];
               
        button.frame = CGRectMake(60.0, 155.0, 200.0, 90.0);
        [self.view addSubview:button];
        ////////
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                   action:@selector(pushEmer:)
         forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"Botón-números-de-emergencia.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(60.0, 325.0, 200.0, 90.0);
        [self.view addSubview:button1];
//////////////
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageNamed:@"fondo-serviciosF.png"] drawInRect:(CGRectMake(0, 0, 320, 500))];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];}
    else{
           UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self
                   action:@selector(pushInfo:)
         forControlEvents:UIControlEventTouchDown];
        // [button setTitle:@"Show View" forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"Botón-Información-vehicular.png"] forState:UIControlStateNormal];
        
        button.frame = CGRectMake(60.0, 125.0, 200.0, 90.0);
        [self.view addSubview:button];
        ////////
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(pushEmer:)
          forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"Botón-números-de-emergencia.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(60.0, 265.0, 200.0, 90.0);
        [self.view addSubview:button1];
        
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageNamed:@"fondo-serviciosF.png"] drawInRect:(CGRectMake(0, 0, 320, 420))];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        


    }
    /////////

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)gotoBack:(id)sender
{
    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"nab-bar2.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
//////////
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, -62, 320, 420))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ///////
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)pushEmer:(id)sender{
    
    NumEmergencia *mp = [[NumEmergencia alloc]initWithNibName:@"NumEmergencia" bundle:[NSBundle mainBundle]];
    
    self.emer = mp;
    [self.navigationController pushViewController:mp animated:YES];

    
}

-(IBAction)pushInfo:(id)sender{
    InfoVehicular *mp = [[InfoVehicular alloc]initWithNibName:@"InfoVehicular" bundle:[NSBundle mainBundle]];
    
    self.info = mp;
    [self.navigationController pushViewController:mp animated:YES];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
