//
//  HSDataStore.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-28.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HSDataStore : NSObject

+ (id)shared;

/// @name Controllers management and callbacks
/// --------------------
//- (void)registerController:(id <HSDataStoreDelegate>)controller;

/// Stored Data Types below

/// @name Home Page Widgets
/// --------------------
/** Store Widget */
@property (atomic, strong) NSMutableArray * homePageBanners;
/** Load all widgets */
- (void)loadHomePageBanners;


@end
