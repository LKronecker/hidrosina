//
//  GasDetails.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GasDetails : UIViewController{
    NSArray *adressArray;
    UILabel *ttLabel;
    UITextView *adrsView;
    NSArray *latitudeArray;
    NSArray *longitudeArray;
    UIImageView *diesel;
    UIImageView *premium;
    UIImageView *magna;
    UIImageView *tienda;
    UIImageView *atm;
    UIImageView *tarjeta;
    UIImageView *titulo;
    
    UITextView *direc;

    
    UIView *serviviosView;
    
    BOOL orientseason;
}
@property (nonatomic, retain) NSString *detCount;
@property (nonatomic, retain) NSString *titl;
@property (nonatomic, retain) NSString *longST;
@property (nonatomic, retain) NSString *latST;
@property (nonatomic, retain) NSString *gasST;
@property (nonatomic, retain) NSArray *latitudeArray;
@property (nonatomic, retain) NSArray *longitudeArray;
@property (nonatomic, retain) NSArray *adressArray;
  @property (nonatomic, retain) IBOutlet  UILabel *ttLabel;
@property (nonatomic, retain) IBOutlet  UITextView *adrsView;
  @property (nonatomic, retain) IBOutlet  UIImageView *diesel;
 @property (nonatomic, retain) IBOutlet UIImageView *premium;
 @property (nonatomic, retain) IBOutlet UIImageView *magna;
@property (nonatomic, retain) IBOutlet UIImageView *tienda;
@property (nonatomic, retain) IBOutlet UIImageView *atm;
@property (nonatomic, retain) IBOutlet UIImageView *tarjeta;
@property (nonatomic, retain) IBOutlet UIImageView *titulo;

 @property (nonatomic, retain) IBOutlet  UITextView *direc;


  @property (nonatomic, retain) IBOutlet   UIView *serviviosView;

-(IBAction)pushDemo;

@end
