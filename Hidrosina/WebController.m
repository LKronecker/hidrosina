//
//  WebController.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-04-22.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "WebController.h"

@interface WebController ()

@end

@implementation WebController
@synthesize webView, urlString, activityindicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
     webView.delegate = self;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 85, 35);
    //[button setTitle:@"Regresar" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"BOTÓN-REGRESAR.png"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    /////////
    [webView addSubview:activityindicator];
    
    NSString *urlAddress = urlString;
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    timer = [NSTimer scheduledTimerWithTimeInterval:(1.0/2.0) target:self selector:@selector(loading) userInfo:nil repeats:YES];
    
    [self.view addSubview:webView];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)gotoBack:(id)sender
{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, -62, 320, 480))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ///////
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)loading{
    if(!webView.loading){
        [activityindicator stopAnimating];
        activityindicator.hidden = YES;}
	
    else{
		[activityindicator startAnimating];
        activityindicator.hidden = NO; }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webView:(UIWebView *)webview didFailLoadWithError:(NSError *)error {
    // if (error.code == NSURLErrorNotConnectedToInternet){
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Imposible descargar contenido, verifique su conexión a Internet"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    NSLog(@"Alert");

    //}
}

@end
