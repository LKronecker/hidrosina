//
//  HidrosinaHome.m
//  Hidrosina
//ileana
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "HidrosinaHome.h"
#define IS_WIDESCREEN ( fabs (( double ) [[ UIScreen mainScreen ] bounds].size.height - (double )568 ) < DBL_EPSILON)
#define kNavBarHeight 80.0f

////@interface UINavigationBar (myNave)
//////- (CGSize)changeHeight:(CGSize)size;
////@end
////
////@implementation UINavigationBar (customNav)
////- (CGSize)sizeThatFits:(CGSize)size {
////    CGSize newSize = CGSizeMake(320,kNavBarHeight);
////    return newSize;
////}
////@end
//
//@interface HidrosinaHome ()
//
//@end
//
//typedef enum {
//    ScrollDirectionNone,
//    ScrollDirectionRight,
//    ScrollDirectionLeft,
//    ScrollDirectionUp,
//    ScrollDirectionDown,
//    ScrollDirectionCrazy,
//} ScrollDirection;
//
//@implementation HidrosinaHome
//@synthesize hMaps, fact, serv, tarj, scrollView, scrollViewHD, image0, image1, image2, image3, image4, imageContainer, darkHeader, imageDet, estaciones, servicios, hydroImage, estaciones2, servicios2, timer, silder0, silder1, silder2, silder3, silder4, webView, activityindicator, hydroImage1;
//
//- (void)downloadImage:(NSString *)image inBackground:(UIImageView *)imageView
//{
//    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
//    [activity setCenter:CGPointMake(imageView.bounds.size.width/2, imageView.bounds.size.height/2)];
//    [imageView addSubview:activity];
//    [activity startAnimating];
//    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://i1065.photobucket.com/albums/u395/lkronecker/%@",image]];
////   http://i1065.photobucket.com/albums/u395/lkronecker/0F.png
//    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
//    dispatch_async(download_queue, ^{
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        UIImage *_image = [UIImage imageWithData:data];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [activity stopAnimating];
//            [activity removeFromSuperview];
//            [imageView setImage:_image];
//        });
//    });
//   // dispatch_release(download_queue);
//}
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//     webView.delegate = self;
//    webView.alpha = 0;
//    hydroImage1.alpha = 0;
//    [webView addSubview:activityindicator];
//    
//    NSString *urlAddress = @"http://www.google.com";
//    NSURL *url = [NSURL URLWithString:urlAddress];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    [webView loadRequest:requestObj];
//    timer = [NSTimer scheduledTimerWithTimeInterval:(1.0/2.0) target:self selector:@selector(loading) userInfo:nil repeats:YES];
//    
//    [self.view addSubview:webView];
//    /////
//        hydroImage.alpha = 1;
//    
//    bannerCount = 7;
//    estaciones2.alpha = 0;
//    servicios2.alpha = 0;
//    //////////////////
//    imageContainer.userInteractionEnabled = YES;
//    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap2:)];
//    recognizer.numberOfTapsRequired = 1;
//    recognizer.numberOfTouchesRequired = 1;
//    
//    [imageContainer addGestureRecognizer:recognizer];
//    
//    imageContainer.alpha = 0;
//    
//    ///////////////////////
//   //////////////
//    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
//    [[UIImage imageNamed:@"nab-bar2.png"] drawInRect:(CGRectMake(0, 0, 320, kNavBarHeight))];
//    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
//    
//    if (IS_WIDESCREEN) {
//        UIGraphicsBeginImageContext(self.view.frame.size);
//        [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, kNavBarHeight, 320, 500))];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
//    /////////
//        ///////////
//        CGRect scrollViewFrame = CGRectMake(0, 250, 395, 250);
//        self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
//        
//        [self.view addSubview: self.scrollView];
//        CGSize scrollViewContentSize = CGSizeMake(600, 600);
//        [self.scrollView setContentSize:scrollViewContentSize];
//           scrollView.delegate = self;
//        
//        [self.scrollView setBackgroundColor:[UIColor blackColor]];
//        [scrollView setCanCancelContentTouches:NO];
//        
//        scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
//        scrollView.clipsToBounds = YES;
//        scrollView.scrollEnabled = YES;
//        scrollView.pagingEnabled = YES;
//        scrollView.alpha = 0;
//        scrollView.backgroundColor = nil;
//        
//        NSUInteger nimages = 0;
//        CGFloat cx = 0;
//        for (; ; nimages++) {
//           // NSString *imageName = [NSString stringWithFormat:@"%d.png", (nimages)];
//            
//            //  UIImage *image = [UIImage imageNamed:imageName];
//            //UIImageView *imageView = [[UIImageView alloc] init];
//            
//          //  [self downloadImage:imageName inBackground:imageView];
//            
//           NSString *imageName = [NSString stringWithFormat:@"%lu.png", (unsigned long)(nimages)];
//          //   NSString *imageName = [NSString stringWithFormat:@"fondoWeb.png"];
//            
//            UIImage *image = [UIImage imageNamed:imageName];
//            
//            if (image == nil) {
//                break;
//            }
//            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
//            
//
//            CGRect rect = imageView.frame;
//            rect.size.height = 180;
//            rect.size.width = 297;
//            rect.origin.x = ((scrollView.frame.size.width - (image.size.width))/2) + cx;
//            rect.origin.y = ((scrollView.frame.size.height - image.size.height)/2);
//            
//            imageView.frame = rect;
//            
//            [scrollView addSubview:imageView];
//            // [imageView release];
//            
//            cx += scrollView.frame.size.width;
//            
//            
//            
//            if ([imageName isEqualToString:@"0.png"]) { imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap0:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                self.image0 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
//            if ([imageName isEqualToString:@"1.png"]) { imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap1:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                self.image1 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
//            if ([imageName isEqualToString:@"2.png"]) { imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap2A:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                self.image2 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
//            if ([imageName isEqualToString:@"3.png"]) { imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap3:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                self.image3 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
//            if ([imageName isEqualToString:@"4.png"]) { imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap4:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                
//                self.image4 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
//            
//           
//
//            
//            
//            
//        }
//        
//        
//        [scrollView setContentSize:CGSizeMake(cx, [scrollView bounds].size.height)]; 
//        
//
//    }
//    else{
//        UIGraphicsBeginImageContext(self.view.frame.size);
//        [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, 100, 320, 365))];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
//        ///////
//        
//        CGRect scrollViewFrame = CGRectMake(0, 210, 395, 250);
//        self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
//        
//        [self.view addSubview: self.scrollView];
//        CGSize scrollViewContentSize = CGSizeMake(600, 600);
//        [self.scrollView setContentSize:scrollViewContentSize];
//           scrollView.delegate = self;
//        
//        [self.scrollView setBackgroundColor:[UIColor blackColor]];
//        [scrollView setCanCancelContentTouches:NO];
//        
//        scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
//        scrollView.clipsToBounds = YES;
//        scrollView.scrollEnabled = YES;
//        scrollView.pagingEnabled = YES;
//                scrollView.alpha = 0;
//        scrollView.backgroundColor = nil;
//        
//        NSUInteger nimages = 0;
//        CGFloat cx = 0;
//        for (; ; nimages++) {
//            NSString *imageName = [NSString stringWithFormat:@"%lu.png", (unsigned long)(nimages)];
//           //  NSString *imageName = [NSString stringWithFormat:@"cuadro-de-textoN.png"];
//            UIImage *image = [UIImage imageNamed:imageName];
//            
//            if (image == nil) {
//                break;
//            }
//            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
//            
//            CGRect rect = imageView.frame;
//            rect.size.height = 180;
//            rect.size.width = 297;
//            rect.origin.x = ((scrollView.frame.size.width - (image.size.width))/2) + cx;
//            rect.origin.y = ((scrollView.frame.size.height - image.size.height)/2);
//            
//            imageView.frame = rect;
//            
//            [scrollView addSubview:imageView];
//            // [imageView release];
//            
//            cx += scrollView.frame.size.width;
//            
//            
//            
//            if ([imageName isEqualToString:@"0.png"]) { imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap0:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                self.image0 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
//            if ([imageName isEqualToString:@"1.png"]) { imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap1:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                self.image1 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
//             imageView.userInteractionEnabled = YES;
//                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap2A:)];
//                recognizer.numberOfTapsRequired = 1;
//                recognizer.numberOfTouchesRequired = 1;
//                self.image2 = imageView;
//                
//                [imageView addGestureRecognizer:recognizer];}
////            if ([imageName isEqualToString:@"3.png"]) {
////                imageView.userInteractionEnabled = YES;
////                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap3:)];
////                recognizer.numberOfTapsRequired = 1;
////                recognizer.numberOfTouchesRequired = 1;
////                self.image3 = imageView;
////                
////                [imageView addGestureRecognizer:recognizer];}
////            if ([imageName isEqualToString:@"4.png"]) { imageView.userInteractionEnabled = YES;
////                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap4:)];
////                recognizer.numberOfTapsRequired = 1;
////                recognizer.numberOfTouchesRequired = 1;
////                
////                self.image4 = imageView;
////                
////                [imageView addGestureRecognizer:recognizer];}
//            
//            
//            
//        
//        
//
//        [scrollView setContentSize:CGSizeMake(cx, [scrollView bounds].size.height)];
//        
//
//   
//    }
//    ///
//   // self.navigationController.navigationBar.alpha = 0.1;
//    ///////
//    
//    
//   timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//    
//   // NSTimer *tm = [NSTimer timerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//    
//    
//  //  self.timer = tm;
//    
//    
//    
//    
//    [NSTimer scheduledTimerWithTimeInterval:1.3 target:self selector:@selector(configView:) userInfo:nil repeats:NO];
//    /////////
//    [super viewDidLoad];
//    // Do any additional setup after loading the view from its nib.
//}
//-(void)viewWillDisappear:(BOOL)animated{
//   
//    estaciones2.alpha = 1;
//    servicios2.alpha = 1;
//
//    
//}
//
//-(void)viewDidDisappear:(BOOL)animated{
//    
//    if(timer)
//    {
//        [timer invalidate];
//        timer = nil;
//    }
//   // animated = YES;
//}
//
//
//-(void)loading{
//    if(!webView.loading){
//        [activityindicator stopAnimating];
//        activityindicator.hidden = YES;}
//	
//    else{
//		[activityindicator startAnimating];
//        activityindicator.hidden = NO; }
//}
//
//
//-(IBAction)configView:(id)sender{
//    [UIView beginAnimations:@"advancedAnimations" context:nil];
//	[UIView setAnimationDuration:0.6];
//    
//    CGRect headerFrame = servicios.frame;
//	//ventaFrame.origin.y -= 150;
//    headerFrame.origin.x -= 150;
//	servicios.frame = headerFrame;
//    
//    CGRect headerFrame1 = estaciones.frame;
//	//ventaFrame.origin.y -= 150;
//    headerFrame1.origin.x += 150;
//	estaciones.frame = headerFrame1;
//    scrollView.alpha = 1;
//    hydroImage.alpha = 0;
//       
//    [UIView commitAnimations];
//    
//  
//    
//    [self downloadImage:@"0.png" inBackground:image0];
//    [self downloadImage:@"1.png" inBackground:image1];
//    [self downloadImage:@"2.png" inBackground:image2];
//    [self downloadImage:@"3.png" inBackground:image3];
//    [self downloadImage:@"4.png" inBackground:image4];
//
//}
//
//-(IBAction)slide:(id)sender{
//    
//    if (bannerCount > 1400) {
//        bannerCount = 0;
//        
//        CGPoint bottomOffset = CGPointMake(0, bannerCount);
//        [self.scrollView setContentOffset:bottomOffset animated:NO];
//    }else{
//        
//        bannerCount += 392;
//        
//        CGPoint bottomOffset = CGPointMake(bannerCount, 0);
//        [self.scrollView setContentOffset:bottomOffset animated:YES];}
//    
//    //if (bannerCount == 1) bannerCount = 7;
//    
//    NSLog(@"sirve");
//}
//
//- (void)handleTap0:(UITapGestureRecognizer *)sender {
//    
//        if (sender.state == UIGestureRecognizerStateEnded) {
//            if(timer)
//            {
//                [timer invalidate];
//                timer = nil;
//            }
//            
//
//       [self.imageDet removeFromSuperview];
//        
//        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        self.scrollView. alpha = 0;
//        
//        [UIView beginAnimations:@"Dilation" context:nil];
//        [UIView setAnimationDuration:0.5];
//        imageContainer.alpha = 1;
//        CGRect newFrame = self.view.frame;
//        newFrame.origin.y = 0;
//        newFrame.origin.x = 0;
//        
//        imageContainer.frame = newFrame;
//         
//         self.scrollView. alpha = 0;
//      //  estaciones2.alpha = 0;
//      //  servicios2.alpha = 0;
//        
//        
//        [UIView commitAnimations];
//        
//        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
//        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        //  view.alpha = 0.7;
//        
//        self.darkHeader = view;
//        
//        [self.navigationController.navigationBar addSubview:view];
//       
//        
//         [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails) userInfo:nil repeats:NO];
//           
//       //  [scrollView removeFromSuperview];
//        
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
//        
//        [self downloadImage:@"0F.png" inBackground:imageView];
//        self.imageDet = imageView;
//        
//        [self.imageContainer addSubview:imageDet];
//        
//
//            
//    }
//}
//- (void)handleTap1:(UITapGestureRecognizer *)sender {
//    if (sender.state == UIGestureRecognizerStateEnded) {
//        
//        if(timer)
//        {
//            [timer invalidate];
//            timer = nil;
//        }
//        
//
//         [self.imageDet removeFromSuperview];
//        
//        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        
//        [UIView beginAnimations:@"Dilation" context:nil];
//        [UIView setAnimationDuration:0.5];
//        imageContainer.alpha = 1;
//        CGRect newFrame = self.view.frame;
//        newFrame.origin.y = 0;
//        newFrame.origin.x = 0;
//        
//        imageContainer.frame = newFrame;
//        self.scrollView. alpha = 0;
//        estaciones2.alpha = 0;
//        servicios2.alpha = 0;
//        
//        [UIView commitAnimations];
//        
//        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
//        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        //  view.alpha = 0.7;
//        
//        self.darkHeader = view;
//        
//        [self.navigationController.navigationBar addSubview:view];
//        
//
//        
//        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails1) userInfo:nil repeats:NO];
//        
//               UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
//        
//        [self downloadImage:@"1F.png" inBackground:imageView];
//        self.imageDet = imageView;
//        
//        [self.imageContainer addSubview:imageDet];
//          }
//}
//- (void)handleTap2A:(UITapGestureRecognizer *)sender {
//    if (sender.state == UIGestureRecognizerStateEnded) {
//         [self.imageDet removeFromSuperview];
//        
//        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        
//        [UIView beginAnimations:@"Dilation" context:nil];
//        [UIView setAnimationDuration:0.5];
//        imageContainer.alpha = 1;
//        CGRect newFrame = self.view.frame;
//        newFrame.origin.y = 0;
//        newFrame.origin.x = 0;
//        
//        imageContainer.frame = newFrame;
//        estaciones2.alpha = 0;
//        servicios2.alpha = 0;
//        self.scrollView. alpha = 0;
//        
//        [UIView commitAnimations];
//        
//        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
//        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        //  view.alpha = 0.7;
//        
//        self.darkHeader = view;
//        
//        [self.navigationController.navigationBar addSubview:view];
//        
//
//        
//        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails2) userInfo:nil repeats:NO];
//        
//        if(timer)
//        {
//            [timer invalidate];
//            timer = nil;
//        }
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
//        
//        [self downloadImage:@"2F.png" inBackground:imageView];
//        self.imageDet = imageView;
//        
//        [self.imageContainer addSubview:imageDet];
//
//          }
//}
//- (void)handleTap3:(UITapGestureRecognizer *)sender {
//    if (sender.state == UIGestureRecognizerStateEnded) {
//         [self.imageDet removeFromSuperview];
//        
//        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        
//        [UIView beginAnimations:@"Dilation" context:nil];
//        [UIView setAnimationDuration:0.5];
//        imageContainer.alpha = 1;
//        CGRect newFrame = self.view.frame;
//        newFrame.origin.y = 0;
//        newFrame.origin.x = 0;
//        
//        imageContainer.frame = newFrame;
//        self.scrollView. alpha = 0;
//        
//        estaciones2.alpha = 0;
//        servicios2.alpha = 0;
//        
//        [UIView commitAnimations];
//        
//        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
//        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        //  view.alpha = 0.7;
//        
//        self.darkHeader = view;
//        
//        [self.navigationController.navigationBar addSubview:view];
//        
//
//        
//        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails3) userInfo:nil repeats:NO];
//        
//        if(timer)
//        {
//            [timer invalidate];
//            timer = nil;
//        }
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
//        
//        [self downloadImage:@"3F.png" inBackground:imageView];
//        self.imageDet = imageView;
//        
//        [self.imageContainer addSubview:imageDet];
//
//          }
//}
//- (void)handleTap4:(UITapGestureRecognizer *)sender {
//    if (sender.state == UIGestureRecognizerStateEnded) {
//        
//         [self.imageDet removeFromSuperview];
//        imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        
//        [UIView beginAnimations:@"Dilation" context:nil];
//        [UIView setAnimationDuration:0.5];
//        imageContainer.alpha = 1;
//        CGRect newFrame = self.view.frame;
//        newFrame.origin.y = 0;
//        newFrame.origin.x = 0;
//        
//        imageContainer.frame = newFrame;
//        self.scrollView. alpha = 0;
//        
//        estaciones2.alpha = 0;
//        servicios2.alpha = 0;
//        
//        [UIView commitAnimations];
//        
//        UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
//        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imagen-negra-transparencia.png"]];
//        //  view.alpha = 0.7;
//        
//        self.darkHeader = view;
//        
//        [self.navigationController.navigationBar addSubview:view];
//        
//
//        
//        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(imageDetails4) userInfo:nil repeats:NO];
//        
//        if(timer)
//        {
//            [timer invalidate];
//            timer = nil;
//        }
//        
//
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 320, 320)];
//        
//        [self downloadImage:@"4F.png" inBackground:imageView];
//        self.imageDet = imageView;
//        
//        [self.imageContainer addSubview:imageDet];
//        
//    }
//}
//
//
//-(void) imageDetails{
//    
//   //////
//    
//     //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//    
//
//    
////
//}
//-(void) imageDetails1{
//    
//     //    timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//    
//    
//    //
//}
//-(void) imageDetails2{
//    
//     //   timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//   }
//-(void) imageDetails3{
//     //timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//   
//}
//  -(void) imageDetails4{
//   //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//      
//}
//
//
//
//
//
//- (void)handleTap2:(UITapGestureRecognizer *)sender {
//    if (sender.state == UIGestureRecognizerStateEnded) {
//       // [self.view clearsContextBeforeDrawing];
//       // self.navigationController.navigationBarHidden = NO;
//        
//        [UIView beginAnimations:@"Dilation" context:nil];
//        [UIView setAnimationDuration:1.0];
//        imageContainer.alpha = 0;
//        CGRect newFrame = CGRectMake(45, 334, 230 , 100);
//        
//       // newFrame.origin.y = 146;
//        //newFrame.origin.x = 361;
//        
//        imageContainer.frame = newFrame;
//        
//        estaciones2.alpha = 1;
//        servicios2.alpha = 1;
//        self.scrollView. alpha = 1;
//        
//        [UIView commitAnimations];
//        
//        [self.darkHeader removeFromSuperview];
//        [self.imageDet removeFromSuperview];
//       // [self.view removeFromSuperview];
//        
//         timer = [NSTimer scheduledTimerWithTimeInterval:3.5 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//      //  NSLog(@"WEY!!!");
//    }
//}
//
//
//-(IBAction)pushMaps
//{
//    hidroMapas *mp = [[hidroMapas alloc]initWithNibName:@"hidroMapas" bundle:[NSBundle mainBundle]];
//    
//    self.hMaps = mp;
//    [self.navigationController pushViewController:mp animated:YES];
//    
//    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
//    [[UIImage imageNamed:@"header2.png"] drawInRect:self.navigationController.navigationBar.bounds];
//    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
//    
//
//
//}
//
//-(IBAction)pushServ{
//    
//    Facturacion *mp = [[Facturacion alloc]initWithNibName:@"Facturacion" bundle:[NSBundle mainBundle]];
//    
//    self.fact = mp;
//    [self.navigationController pushViewController:mp animated:YES];
//    
//    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
//    [[UIImage imageNamed:@"header2.png"] drawInRect:self.navigationController.navigationBar.bounds];
//    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
//    
//    
//
//
//}
//
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//-(void)webView:(UIWebView *)webview didFailLoadWithError:(NSError *)error {
//
//    // if (error.code == NSURLErrorNotConnectedToInternet){
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                    message:@"Imposible descargar contenido, verifique su conexión a Internet."
//                                                   delegate:self
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
//    NSLog(@"Alert");
//    
//   // }
//  //  [self.view sendSubviewToBack:scrollView];
//    [self.scrollView removeFromSuperview];
//    
//        hydroImage1.alpha = 1;
//    estaciones2.alpha = 1;
//    servicios2.alpha = 1;
//    
//  //  [image0 setImage:[UIImage imageNamed:@"new0.png"]];
//   // [image1 setImage:[UIImage imageNamed:@"new1.png"]];
//    //[image2 setImage:[UIImage imageNamed:@"new2.png"]];
//    //[image3 setImage:[UIImage imageNamed:@"new3.png"]];
//    //[image4 setImage:[UIImage imageNamed:@"new4.png"]];
//    
//    
//    
//}
//
//
//#pragma mark -
//#pragma mark Scroll View Delegate Methods
//
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollV
//{
//      
//
//  //  lastScrollPosition->x = scrollV.contentOffset.x / 55;
//     //  NSLog(@"damn");
//  //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//    
//    if(timer)
//    {
//        [timer invalidate];
//        timer = nil;
//    }
//    
////  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//
//}
//
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
// 
//     timer = [NSTimer scheduledTimerWithTimeInterval:4.5 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//    
//}
//-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
//
//    //  timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//}
//
//- (void)scrollViewDidScroll:(UIScrollView *)sender
//{
//  //   timer = [NSTimer scheduledTimerWithTimeInterval:4.4 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
//    
// /*   ScrollDirection scrollDirection;
//    if (self.lastContentOffset > scrollView.contentOffset.x){
//        scrollDirection = ScrollDirectionRight;
//       //  bannerCount -= 392;
//       //  NSLog(@"Right");
//    }
//    else if (self.lastContentOffset < scrollView.contentOffset.x){
//        scrollDirection = ScrollDirectionLeft;
//     //    bannerCount += 392;
//       //  NSLog(@"Left, %i", bannerCount);
//    }
//    self.lastContentOffset = scrollView.contentOffset.x;
//    
//    // do whatever you need to with scrollDirection here. */
//}

//@end
