//
//  DetailViewController.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapAnotations.h"
#import "GasDetails.h"
#import "HidrosinaHome.h"

@class GasDetails, HidrosinaHome;


@interface DetailViewController : UIViewController <UIScrollViewDelegate, UISplitViewControllerDelegate,UITabBarDelegate, MKMapViewDelegate, UIWebViewDelegate, CLLocationManagerDelegate >{
    
    BOOL orientseason;
    
    UITabBarItem *mapTab;
    
    UIImageView *image0;
    UIImageView *image1;
    UIImageView *image2;
    UIImageView *image3;
    UIImageView *image4;
    
     int bannerCount;
    
    UIView *imageContainer;
    NSTimer *timer;
    UIImageView *imageDet;

    
     UIWebView *HIDROwebView;
    UITabBar *tabbar;
    MKMapView *mapView;
    GasDetails *gdet;
    HidrosinaHome *home;
    NSArray *gasArray;
    NSArray *subtArray;
    NSArray *latitudeArray;
    NSArray *longitudeArray;
    NSArray *tittleArray;
    UISegmentedControl *control;
    
    UISegmentedControl *RangeControl;
    
    UIButton *km1Button;
    UIButton *km2Button;
    UIButton *km3Button;
    UIButton *km4Button;
    
    UIWebView *webView;
    UIWebView *webView1;
    UIActivityIndicatorView *activityindicator;
    UIScrollView *scrollView;
}

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
 

////
@property (nonatomic, retain) UIImageView *image0;
@property (nonatomic, retain) UIImageView *image1;
@property (nonatomic, retain) UIImageView *image2;
@property (nonatomic, retain) UIImageView *image3;
@property (nonatomic, retain) UIImageView *image4;


@property (nonatomic, retain)  UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIWebView *HIDROwebView;
@property (nonatomic, retain) IBOutlet    UITabBarItem *mapTab;

@property (nonatomic, retain) IBOutlet UITabBar *tabbar;

@property (nonatomic, retain) IBOutlet UISegmentedControl *control;
@property (nonatomic, retain) IBOutlet UISegmentedControl *RangeControl;
@property (nonatomic, retain) IBOutlet UIButton *km1Button;
@property (nonatomic, retain) IBOutlet UIButton *km2Button;
@property (nonatomic, retain) IBOutlet UIButton *km3Button;
@property (nonatomic, retain) IBOutlet UIButton *km4Button;
@property (nonatomic, retain)  NSArray *latitudeArray;
@property (nonatomic, retain)  NSArray *longitudeArray;
@property (nonatomic, retain)  NSArray *tittleArray;
@property (nonatomic, retain)   NSArray *gasArray;
@property (nonatomic, retain) MKMapView *mapView;
@property (nonatomic, retain) NSArray *subtArray;
@property (nonatomic, retain)   GasDetails *gdet;
@property (nonatomic, retain)  HidrosinaHome *home;

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIWebView *webView1;
@property (nonatomic, retain) IBOutlet     UIActivityIndicatorView *activityindicator;

@property (nonatomic, retain) UIImageView *imageDet;
@property (nonatomic, retain)  NSTimer *timer;
@property (nonatomic, retain) IBOutlet  UIView *imageContainer;


-(IBAction)places:(id)sender;
-(IBAction)showDetails:(id)sender;
-(IBAction)changeSeg:(id)sender;
-(IBAction)pushDemo;
-(IBAction)changeRange:(id)sender;

-(IBAction)goLoc:(id)sender;
-(IBAction)goLoc1:(id)sender;
-(IBAction)goLoc2:(id)sender;
- (void)gotoLocation;
- (void)gotoLocation1;
- (void)gotoLocation2;
-(void)loading;

-(IBAction)goLoc3:(id)sender;

@end
