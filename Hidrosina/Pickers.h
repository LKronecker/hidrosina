//
//  Pickers.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-04-26.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebController.h"

@class WebController;


@interface Pickers : UIViewController<UIPickerViewDelegate>{
    NSArray *brandArray;
    NSArray *seguroArray;
    NSArray *urlSegArray;
    
    UIPickerView *brandPicker;
    UIPickerView *segPicker;
   
    NSArray *urlArray;
    WebController *webCont;
}

@property (nonatomic, retain)  NSArray *brandArray;
@property (nonatomic, retain) IBOutlet    UIPickerView *brandPicker;
@property (nonatomic, retain) IBOutlet   UIPickerView *segPicker;
@property (nonatomic, retain) IBOutlet  UIView *pickerView;
@property (nonatomic, retain)  NSArray *seguroArray;
@property (nonatomic, retain)  NSArray *urlSegArray;
@property (nonatomic, retain) NSArray *urlArray;
@property (nonatomic, retain)  WebController *webCont;

@end
