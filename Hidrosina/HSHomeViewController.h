//
//  HSHomeViewController.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-27.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSHomeViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

-(IBAction)pushWebSite;
-(IBAction)openFaceBookPage;
- (IBAction)openWebSite:(id)sender;

@end
