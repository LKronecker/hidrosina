//
//  GasDetails.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "GasDetails.h"
#define IS_WIDESCREEN ( fabs (( double ) [[ UIScreen mainScreen ] bounds].size.height - (double )568 ) < DBL_EPSILON)
@interface GasDetails ()

@end

@implementation GasDetails
@synthesize detCount, adressArray, ttLabel, adrsView, titl, longST, latST, gasST, diesel, longitudeArray, latitudeArray, serviviosView, premium, magna, tienda, atm, tarjeta, titulo, direc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
              
        
     //   UIGraphicsBeginImageContext(adrsView.frame.size);
      //  [[UIImage imageNamed:@"cuadro-de-textoN.png"] drawInRect:adrsView.bounds];
      //  UIImage *imageC = UIGraphicsGetImageFromCurrentImageContext();
       // UIGraphicsEndImageContext();
        
       // adrsView.backgroundColor = [UIColor colorWithPatternImage:imageC];

        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    //[self viewDidAppear:YES];
   // [self orientationchngfn];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
        UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
        {
            UIGraphicsBeginImageContext(self.view.frame.size);
            [[UIImage imageNamed:@"Fondo_iPad.png"] drawInRect:(CGRectMake(0, 0, 768, 1004))];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
            
            titulo.frame = CGRectMake(288, 458, 239, 47);
            
            tienda.frame = CGRectMake(581, 544, 90, 90);
            tarjeta.frame = CGRectMake(362, 544, 90, 90);
            atm.frame = CGRectMake(121, 544, 90, 90);
        }
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation))
        {
            orientseason = 1;
            
            
            UIGraphicsBeginImageContext(CGSizeMake(1024, 768));
            [[UIImage imageNamed:@"Fondo-landscape-2008-x1536.png"] drawInRect:(CGRectMake(0, 0, 1024, 768))];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
            
            
            
            titulo.frame = CGRectMake(50, 500, 239, 47);
            tarjeta.frame = CGRectMake(240, 550, 90, 90);
            atm.frame = CGRectMake(50, 550, 90, 90);
            tienda.frame = CGRectMake(140, 600, 90, 90);
            

            
        }
            
        }

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    
       

    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 85, 35);
    //[button setTitle:@"Regresar" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"BOTÓN-REGRESAR.png"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    //////
    
    if(IS_WIDESCREEN){
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-gasolineriasF.png"] drawInRect:(CGRectMake(0, 0, 320, 509))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        //////
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(goToMaps)
          forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"botón-hidrosina-go!.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(79, 263, 162, 63);
        
        //////////
        [self.view addSubview:button1];
    }else{
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageNamed:@"fondo-gasolineriasF.png"] drawInRect:(CGRectMake(0, 0, 320, 420))];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(goToMaps)
          forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"botón-hidrosina-go!.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(79, 265, 162, 63);
        
        [self.view addSubview:button1];

    }
    }
    else{
        UIGraphicsBeginImageContext(self.direc.frame.size);
        [[UIImage imageNamed:@"cuadro-de-textoN.png"] drawInRect:(self.direc.frame)];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [direc setBackgroundColor:[UIColor colorWithPatternImage:image]];
        
        
        UIDeviceOrientation dorientation =[UIDevice currentDevice].orientation;
        
        if(UIDeviceOrientationIsLandscape(dorientation)){
            
          UIGraphicsBeginImageContext(CGSizeMake(1024, 768));
            [[UIImage imageNamed:@"Fondo-landscape-2008-x1536.png"] drawInRect:(CGRectMake(0, 0, 1024, 768))];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
            
                   }

            
            if(UIDeviceOrientationIsPortrait(dorientation))
                
            {
                UIGraphicsBeginImageContext(self.view.frame.size);
                [[UIImage imageNamed:@"Fondo_iPad.png"] drawInRect:(CGRectMake(0, 0, 768, 1004))];
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                self.view.backgroundColor = [UIColor colorWithPatternImage:image];
            }
        
        
                
        ////
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0, 0, 85, 35);
        //[button setTitle:@"Regresar" forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"BOTÓN-REGRESAR.png"] forState:UIControlStateNormal];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
        self.navigationItem.leftBarButtonItem = backButton;

    }

    //////
      diesel.hidden = YES;
    NSLog(@"???%@", gasST);
   
    if ([gasST isEqualToString: @"1"]){diesel.hidden = NO;}
    NSArray *adrs = [[NSArray alloc]initWithObjects:@"Camino Real A Toluca No. 521, Santa Fé, 01140, Álvaro Obregón, Distrito Federal",@"Insurgentes Sur No. 2075, Esq. La Paz, San Ángel, 01000, Álvaro Obregón, Distrito Federal",@"Xola No. 18, Álamos, 03400, Benito Juárez, Distrito Federal",@"Tlalpan No. 1395, Portales, 03300, Benito Juárez, Distrito Federal",@"Alfonso XIII y Obrero Mundial, Álamos, 03400, Benito Juarez, Distrito Federal",@"Murillo No. 21, San Pedro de Los Pinos, 03800, Benito Juárez, Distrito Federal",@"Div. del Norte No. 804, Del Valle, 03100, Benito Juárez, Distrito Federal",@"Av. Coyoacán No.107, Esq. Providencia y Xola, Del Valle, 03100, Benito Juárez, Distrito Federal",@"Div. del Norte E Insurgentes Sur No.553, Del Valle, 03100, Benito Juárez, Distrito Federal",@"Calz. de Tlalpan No. 842 Esq. Sur 122, Villa De Cortés, 03530, Benito Juárez, Distrito Federal",@"Dr. Vertiz No. 640, Vertiz Narvarte, 03600, Benito Juárez, Distrito Federal",@"Av. Universidad No. 1291, Esq. Coyoacan, Acacias, 03240, Benito Juárez, Distrito Federal",@"Centenario No. 13 Esq. Belisario Domínguez, Del Carmen Coyoacan, 04000, Coyoacan, Distrito Federal",@"Cuauhtémoc No. 187 , Esq. Querétaro y Frontera, Roma, 06700, Cuauhtémoc, Distrito Federal",@"Eje Lázaro Cárdenas No. 199, Esq. G. Bocanegra, Peralvillo, 06220, Cuauhtémoc, Distrito Federal",@"Nuevo León No. 8 , Esq. Sonora, Hipódromo Condesa, 06170, Cuauhtémoc, Distrito Federal",@"Cedro No. 238, Esq. Carpio, Santa Ma. La Rivera, 06400, Cuauhtémoc, Distrito Federal",@"Campeche No. 214, Esq. Insurgentes Sur, Hipódromo Condesa, 06170, Cuauhtémoc, Distrito Federal",@"Guerrero No.284 y R. Flores Magón, Guerrero, 06300, Cuauhtémoc, Distrito Federal",@"Yucatán No. 125, Esq. Chiapas y Tonalá, Roma, 06700, Cuauhtémoc,Distrito Federal",@"Calzada Guadalupe No. 52, Ex Hipódromo de Peralvillo, 06250, Cuauhtémoc, Distrito Federal",@"Av. Chapultepec y Veracruz No.2, Hipódromo Condesa, 06170, Cuauhtémoc, Distrito Federal",@"Morelia y Puebla No. 137, Roma, 06700, Cuauhtémoc, Distrito Federal",@"Insurgentes y Popocatepetl No. 289, Hipódromo Condesa, 06100, Cuauhtémoc, Distrito Federal",@"Av. Insurgentes Sur No. 541 y Nuevo León, Hipódromo Condesa, 06170, Cuauhtémoc, Distrito Federal",@"Villalongín No. 6 Esq. Insurgentes y Reforma, Cuauhtémoc, 06500, Cuauhtémoc, Distrito Federal",@"Barcelona No.37, Esq. Versalles, Juárez, 06600, Cuauhtémoc, Distrito Federal",@"Guillermo Prieto No. 130, Esq. Melchor Ocampo, San Rafael, 06470, Cuauhtémoc, Distrito Federa",@"Fray Servando Teresa de Mier No. 299, Col. Transito, 6820, Cuauhtémoc, Distrito Federal",@"Montevideo No. 2, Esq. Calzada de Los Misterios, Tepeyac Insurgentes, 07020, Gustavo A. Madero",@"Necaxa, Esq. Calzada de Guadalupe No. 516, Industrial, 07800, Gustavo A.Madero, Distrito Federal",@"Av. Instituto Politécnico Nacional No. 1881, Lindavista, 07300, Gustavo A.Madero, Distrito Federal",@"Calzada Vallejo No. 1055 y Poniente, Nueva Vallejo, 07750, Gustavo A.Madero, Distrito Federal",@"Calzada de Guadalupe No. 312, Vallejo, 07800, Gustavo A.Madero, Distrito Federal",@"Plutarco Elías Calles No.1 y Río Churubusco, Granjas México, 08400, Iztacalco, Distrito Federal.",@"Plutarco Elías Calles no. 1022, Esq. Icacos, Reforma Iztaccihuatl, 08810, Iztacalco, Distrito Federal",@"Coruña No. 452, Esq. La Viga, Viaducto Piedad, 62000, Iztacalco, Distrito Federal",@"Km. 17.5 Autopista México - Puebla, Santiago Acahualtepec, 09600, Iztapalapa, Distrito Federal",@"Calle Tolteca No. 69, Central De Abasto, 09040, Iztapalapa, Distrito Federal",@"Av. Ermita Iztapalapa No. 3389, Santa María Aztahuacan, 09500, Iztapalapa, Distrito Federal",@"Calz. Javier Rojo Gómez No. 462, Guadalupe del Moral, 09300, Iztapalapa, Distrito Federal",@"Calz. Ermita Iztapalapa No. 3048, Sta. Cruz, Meyehualco, 09700, Iztapalapa, Distrito Federal",@"Calz. Ermita Iztapalapa No. 2710, Sta. Cruz, Meyehualco, 09700, Iztapalapa, Distrito Federal",@"Martí No. 210 y Prosperidad, Escandón, 11800, Miguel Hidalgo, Distrito Federal",@"Iturrigaray No. 116 y Virreyes, Lomas de Chapultepec, 11000, Miguel Hidalgo, Distrito Federal",@"Martí, Esq., Carlos B. Zetina No. 117, Escandón, 11800, Miguel Hidalgo, Distrito Federal",@"Lago Argentina No. 7 y Calz. México Tacuba, Argentina, 11270, Miguel Hidalgo, Distrito Federal",@"Protasio Tagle No. 2 y 4, Esq. Maderos, San Miguel Chapultepec, 11850, Miguel Hidalgo, Distrito Federal",@"Av. Río San Joaquín No. 5976, Esq. Lago Ginebra, Popo, 11480, Miguel Hidalgo, Distrito Federal",@"Mar Mediterráneo y Okostsk, Tacuba, 11410, Miguel Hidalgo, Distrito Federal",@"Calz. Tulyehualco No. 5661 Km. 19.5, San Lorenzo Tezonco, 09900, Tlahuac, Distrito Federal",@"Fco. Morazán y General Anaya No. 175, Merced Balbuena, 15810, Venustiano Carranza, Distrito Federal",@"José Joaquín Herrera No.110, Esq. Av. del Trabajo, Morelos, 015270, Venustiano Carranza, Distrito Federal",
                     
                     @"Km. 31.200 Carr. Federal México - Puebla, El Centro, 56530, Ixtapaluca, México",@"Av. De Los Maestros No. 47, Ampliación San Andrés Atenco, 54040, Tlalnepantla, México",@"Km. 42.+500 Carretera Federal Mex - Puebla, Río Frío, Estado De México, 56590, Ixtapaluca, México",@"Av. Central No.20, 3era. Secc. Valle De Aragón, 55325, Ecatepec de Morelos, México",@"Km. 31.5 Carretera México- Cuautitlan, Loma Bonita, 54800, Cuautitlan de Romero Rubio, México",@"Calz. de Las Armas Mz.1 Lot.10 No.17-A, Industrial Las Armas, 54080, Tlalnepantla de Baz, México",@"Km 11.5 Carretera México - Pachuca, San José Ixhuatepec, 54180, Tlalnepantla de Baz,México",@"Av. Baja California # 148, Barrio de San Agustin, 56343, Chimalhuacán, México",@"Boulevard del Lago No. 1-A Mz 1 Lte 3, Col. Geovillas de Terranova, 55883, Acolmán, México",@"Av. Nezahualcoyotl No 172, Col. Santa María Nativitas, 56330, Chimalhuacán , México",@"Vía Adolfo López Mateos S/N, Col. Miguel Hidalgo, 55490, Ecatepec de Morelos, México",@"Calle Palomas esq. Faisanes Mz 1 Lte 1, Col. La Veleta, 55055, Ecatepec de Morelos, México",@"Vía José López Portillo No. 37, Periodistas, 55700, Coacalco de Berriozábal, México",@"Av. Revolución S/N, Hogares María, 55000, Ecatepec de Morelos, México",@"Av. Gustavo Baz no. 4001, Industrial San Nicolás, 54030, Tlalnepantla de Baz, México",@"Vía Gustavo Baz No. 230, Loc. 31 y 41, Tequesquinahuac, 54020, Tlalnepantla de Baz, México",@"Vía Morelos No. 170, Fracc. Los Laureles, 55090, Ecatepec de Morelos, México",@"Av. Manuel de La Peña No. 148, Darío Martínez, 56619, Valle de Chalco, México",
                     
                     @"Km 24.5 de Carretera Transpeninsular, Los Cabos, Cerro Colorado, CP. 23400, San José del Cabo, Baja California",@"Calle Julio Pimentel S/N, Las Veredas, 23400, San José del Cabo, Baja California",@"Leona Vicario S/N esq. Felix Ortega, Manzana 218, Lote 04, Mariano Matamoros, 23468, Los Cabos, Baja California",@"Carretera Transpeninsular Km 35.7 y Camino de Acceso s/n, Santa Rosa, 23400, San José del Cabo, Baja California",@"Av. De las Brisas No. 3013, Brisas del Pacífico, 23410, Cabo San Lucas, Baja California",@"Boulevard Forjadores de Sudcalifornia s/n, El Calandrio, 23085, La Paz, Baja California",@"Calle Manuel Abasolo S/N, El Manglito, 23060, La Paz, Baja California",@"Luis Donaldo Colosio S/n, E. Serdan y G. Prieto, Inalapa, 23098, La Paz, Baja California",@"Calle Prolongación Leona Vicario Fracción B, parcela 101 zzp1, Mesa Colorada, 23410, Cabo San Lucas, Baja California",@"Tercera Nte. y Segunda Ote. No.11, Mpio. De Arriaga, Chiapas, 030450, Arriaga, Chiapas",@"Av. Ejercito Nacional No.5230, Col. Benito Juárez, 32390, Juárez, Chihuahua",@"Blvd. Oscar Tijerina No. 6751, Nuevo Hipódromo, 32674, Juárez, Chihuahua",@"L. J. Fuentes Rodríguez No. 595, Fracc. La Amistad, 26300, Acuña, Coahuila",@"Periférico Luis Echeverría No.999, Lourdes, 25070, Saltillo, Coahuila",@"Blvd. Torreón - Matamoros Esq. Div. Del Norte S/N, Fracc. La Merced, 27270, Torreón, Coahuila",@"Blvd. Independencia Ote #1100, Col Centro, 27000, Torreón, Coahuila",@"Blvd Constitución #1111, Col. Margaritas, 27130, Torreón, Coahuila",@"Juan Pablo Rodríguez 730, Col. Doctores, 25210, Saltillo, Coahuila",@"Aguascalientes 1385, Col. Repúbica, 25000, Saltillo, Coahuila",@"Magnolia No.100, Jardines de Durango, 34220, Durango, Durango",@"Av. Heroico Colegio Militar No. 109, Nueva Vizcaya, 34080, Durango, Durango",@"Blvd. Juan Alonso de Torres #3702 Esq. Blvd. Morelos, San Cayetano de Medina, 37235, León, Guanajuato",@"Avenida 12 de Octubre #200 Esq. Camino San José de Guanajuato, Fracc. Celaya, 38020, Guanajuato",@"Ave. Obregón Sur No.704, Cruz Con Domingo Lizarde, El Edén Salamanca, 36780, Salamanca, Guanajuato",@"Av. Costera Miguel Alemán No. 379, Barrio de La Pinzona, 39360, Acapulco de Juárez, Guerrero",@"Luis Donaldo Colosio No.1501, S/C, 42083, Pachuca De Soto, Hidalgo",@"Blvd. Tula Iturbe # 118, Esq. Tulipanes , El Salitre, 42800, Tula de Allende, Hidalgo",@"Carretera México-Pachuca S/N, Nacozari, 43808, Tizayuca, Hidalgo",@"Boulevard Luis Donaldo Colosio no. 2003, Ex-Hacienda de Coscotitlan, 42080, Pachuca De Soto, Hidalgo",@"Av. Hidalgo No.705 Entre Calz. del Federalismo y Calle Mezquitan, Centro, Sector Hidalgo, 44100, Guadalajara, Jalisco",@"Av. Juárez No.850 Entre Calle Rayón y Jesús L. Camarena, Americana, Sector Hidalgo, 044160, Guadalajara, Jalisco",@"Crucero de Tezoyuca Tepetzingo S/N, Mun. E. Zapata, Mor., 62720, Emiliano Zapata, Morelos",@"No Reelección 385, San Francisco, 62720, Emiliano Zapata, Morelos",@"Km. 10.1 Carretera Federal México-Cuautla, Poblado Tejalpa, 62550, Jiutepec, Morelos",@"Libramiento Cuautla-Izucar de Matamoros Km. 4+300 S/N, Juan Morales,62745, Yecaplixtla, Morelos",@"Libramiento Cuautla-Izucar de Matamoros Km. 4+300 S/N, Juan Morales, 62745, Yecaplixtla, Morelos, ",@"Libramiento Cuautla-Izucar de Matamoros Km. 0.2 No.5, Cuautlizco, 62757, Cuautla, Morelos",@"Libramiento Cuautla-Izucar de Matamoros Km. 5+050 S/N, Juan Morales, 62745, Yecaplixtla, Morelos",@"Carretera Nacional No. 7877, La Estanzuela, 64988, Monterrey, Nuevo León",@"Urano No.2620, Puerta del Sol, 66358, Santa Catarina, Nuevo León",@"Carretera Monterrey - Reynosa No.101, 29 de Julio, 67196, Guadalupe, Nuevo León",@"Carretera México- Tuxpan Km. 183.5, Venta Chica, 73160, Huauchinango, Puebla",@"Héroes del 5 de Mayo No.1101, Santa María, 72080, Puebla, Puebla",@"Av. Salvador Nava No.405, Col. Fraccionamiento Central, 78399, San Luis Potosí, San Luis Potosí",@"Emiliano Zapata Pte. No. 2151, El Vallado, 80110, Culiacán, Sinaloa",@"Blvd. Benjamín Hill No.5720, Infonavit, 80190, Culiacán, Sinaloa",@"Av. Patria, Esq. Blvd Luis F. Molina No.53140, Lázaro Cárdenas, 80290, Culiacán, Sinaloa",@"Periférico Carlos Pellicer No.205, Plutarco Elías Calles, 86090, Centro, Tabasco",@"Ave. Carlos Pellicer S/N, 18 De Marzo, 86140, Villahermosa, Tabasco",@"Periférico Carlos Molina S/N ,Esq. Con Carretera Federal Huimanguillo, Ranchería Paso, 86540, Cárdenas, Tabasco",@"Carretera Federal a Paraíso No.420, López Mateos, 86630, Comalcalco, Tabasco",@"Carretera Juárez Chiapas a Dos Bocas Paraíso S/N, Tlajomulco, 86690, Cundoacán, Tabasco",@"Blvd. Hidalgo Km. 101 S/N, Fracc. Las Fuentes Reynosa, 88710, Reynosa, Tamaulipas",@"Av. Reforma S/N, Campestre, 88278, Nuevo Laredo, Tamaulipas",@"Carretera Tampico Mante No. 6905, Francisco Javier Mina, 89318, Tampico, Tamaulipas",@"Blvd. Virreyes S/N, Fracc. Contry en Reynosa, 88620, Reynosa, Tamaulipas",@"Vicente Guerrero No.900, Villa Juárez, 89860, Mante, Tamaulipas",@"Blvd. Puerto Industrial Primex No.105, Centro, 89600, Altamira, Tamaulipas",@"Prol. González y Av. Leyes De Reforma No. 2035, Parque Industrial, Matamoros Tamaulipas",@"Carretera Tlaxcala a Ocotlan #20, Ocotlan, 90100, Tlaxcala, Tlaxcala",@"Autopista de cuota Arco Norte Km. 98+345.659 al 98+706.346 del cuerpo de Atlacomulco a San Martín Texmelucan, Distrito de Ocampo, 90200, Calpulalpan, Tlaxcala",@"Autopista de cuota Arco Norte Km. 98+706.346 al 98+345.659 del cuerpo de San Martín Texmelucan a Atlacomulco, Distrito de Ocampo, 90200, Calpulalpan, Tlaxcala",@"Avenida Puebla No.502, Palma Sola, 93320, Poza Rica, Veracruz",@"Blvd. Ruiz Cortinez No.1350, Fracc. S.U.T.S.E.M., 94299, Boca Del Rio, Veracruz",@"Av. Las Palmas Nte. No. 101, Paraíso, 96520, Coatzacoalcos, Veracruz",@"Blvd. Ávila Camacho #91, Cuauhtémoc, 96880, Minatitlán, Veracruz",@"Ave. Oriente 31 y Norte 4 #263, Centro, 94300, Orizaba, Veracruz",
                     
                     @"Bl Miguel Aleman n 105, Francisco I Madero, San Pedro Totoltepec, Toluca, México. C.P.: 50200",
                     @"Av Adolfo Lopez Mateos n 1226, San Mateo Oxtotitla, Toluca, Toluca, México. C.P.: 05100",
                     @"Av José López Portillo N 100, San Lorenzo Tepaltitlan, Toluca, Toluca, México. C.P.: 50010",
                     @"CA 5 N 1A, Fraccionamiento Industrial Alce Blanco, Naucalpan de Juárez, Naucalpan, México. C.P.: 53370",
                     @"Carretera México Texcoco Km 28.5, San José, Chicoloapan de Juárez, San Vicente Chicoloapa, Chicoloapan, México. C.P.: 56500",
                     @"Carretera Libre México Puebla Km. 18, Los Reyes Acaquilpan, La Paz, Paz, La,  México. C.P.: 56400",
                     @"Km 27.0 México-Cuautitlán, Lecheria, Tultitlán de Mariano Escobedo, Tultitlán, México. C.P.: 59940",
                     @"Bl Manuel Avila Camacho N 3067, Centro Industrial Tlalnepantla, Tlalnepantla de Baz, Tlalnepantla, Mexico. C.P.: 54020",
                     @"Olmo Num. 125, Valle de los Pinos, Tlalnepantla de Baz, Tlalnepantla, México. C.P.: 54040",
                     @"Autopista México-Querétaro Km 37, Hacienda del Parque sección II, Cuautitlán Izcalli, Cuautitlán Izcalli, México. C.P.: 54769",
                     @"Ca Doctor Carmona Y Valle N 48, Doctores, Cuauhtémoc, Iztacalco, Distrito Federal. C.P.: 06720",
                     @"Calle Bolivar esq. San Jerónimo No. 102, Centro, Cuauhtemoc, Iztacalco, Distrito Federal. C.P.: 06010",
                     @"Boulevard Adolfo López Mateos Num. 17 y 37, El Potrero, Atizapán, Atizapán, México. C.P.: 52975",
                     @"México - Toluca Km:, San Pedro Totoltepec, Toluca, México. C.P.: 50200",
                     @"Av del Norte no. 176, Km 0.000, Atitalaquia, Hidalgo C.P.: 42900",
                     @"Miguel Hidalgo No. 24, Granjas Lomas de Guadalupe, Cuautitlán-Izcalli, Cuautitlán-Izcalli, México. C.P.: 52140",
                     @"Alfredo del Mazo esquina con Industria Minera, San Juan Buenaventura, Toluca de Lerdo (C), Toluca, México. C.P.: 50070",
                     @"Parcela 124 Z 1 P-1 S/N, Ejido de Alpuyeca, Alpuyeca, Xochitepec, Morelos. C.P.: 62790",
                     @"Av Chalma S/N, esq. Av de las torres mz. C-3, fracc. Infonavit Norte, Cuautitlán-Izcalli, Cuautitlán-Izcalli, México. C.P.: 54715",
                     @"Av Vía Morelos N 300, Santa María Tulpetlac, Ecatepec de Morelos, Ecatepec, México. C.P.: 55400",
                     @"Av. Javier Rojo Gómez No. 482, Guadalupe Moral, Iztapalapa, Milpa Alta, Distrito Federal. C.P.: 09300",
                     @"Boulevard Toluca - Metepec 126 nte, La Purisima, Metepec, Metepec, México. C.P.: 52140",
                     @"Av. Aguascalientes No. 7038, Primo Verdad Aguascalientes, Aguascalientes CP 20130",
                     @"Lateral Camino a Mompani #2001 Col. Ejido Tlacote el bajo Municipio de Querétaro, Querétaro.",
                     @"Autopista Guadalajara - Tepic No. 300 a la altura del kilómetro 90+700 en Ixtlan del Río, Nayarit",
                     @"Prol. Paseo del Puerto No. 795, esq. Paseo del Sol, Ex Hacienda Buena  Vista, Veracruz, Veracruz, C.P. 91726.",
                     @"Av. Huehuetoca sin número esquina arcangel San Miguel Col. Claustros de San Miguel, Municipio de Cuautitlan Izcalli",
                     @"Prolongación Av. 1 # 3511-A Col. Zona Industrial, Córdoba, Veracruz, CP 94690",
                     @"Boulevard Días Ordaz No. 182 Col. Coapante, Tapachula Chiapas CP 30740",
                     @"Km. 78+100 al norte con dirección a Atlacomulco de la Autopista de Cuota Arco Norte, Municipio de Tula de Allende, Edo. de Hidalgo",
                     @"Km. 78+100 al sur con dirección a San Martín Texmelucan de la Autopista de Cuota Arco Norte, Municipio de Tula de Allende, Edo. de Hidalgo",
                     @"Carretera Veracruz – El Tejar No. 6114, Col. El Tejar, C.P. 94273, Medellín, Veracruz",
                     @"Km 118.65, Autopista Cuota Mazatlán - Tepic S/N, Municipio de Tecuala, Nayarit, C.P. 63440",
                     @"Km 118.18, Autopista Cuota Tepic - Mazatlán S/N, Municipio de Tecuala, Nayarit, C.P. 63440",
                     @"Gustavo Baz No. 4483, Col. San Pedro Barrientos, Municipio de Tlalnepantla de Baz, C.P. 54110, Estado de México",
                     @"Boulevard Toluca No. 9 Col. San Francisco Cuautlalpan, Municipio de Naucalpan de Juárez, C.P. 53569, Estado de México",
                     @"Manzana 12, Área de Servicios Complementarios, Central de Abastos, Del. Iztapalapa, C.P 09040  Distrito Federal",
                     @"Av. Ejército Mexicano No. 26, Col. Ejido 1° de Mayo, C.P. 94298, Boca del Río, Veracruz",
                     @"Boulevard Mompani #30, Col. La Peña C.T.M., Querétaro, Santiago de Querétaro, C.P. 76117",

                     

                     
                                         
   nil];
    self.adressArray = adrs;
    
    NSArray *lat = [[NSArray alloc]initWithObjects:@"19.390212",@"19.347666",@"19.394318",@"19.367536",@"19.402145",@"19.379434",@"19.392335",@"19.39716",@"19.399388",@"19.388557",@"19.397625",@"19.360324",@"19.350575",@"19.415706",@"19.446566",@"19.4155568",@"19.45128",@"19.408994",@"19.452316",@"19.412837",@"19.456452",@"19.420268",@"19.423993",@"19.415245",@"19.401137",@"19.431589",@"19.428546",@"19.438543",@"19.423044",@"19.485052",@"19.474917",@"19.495099",@"19.487613",@"19.467168",@"19.39717",@"19.388626",@"19.401602",@"19.357308",@"19.372504",@"19.346294",@"19.370565",@"19.343614",@"19.343281",@"19.401471",@"19.422217",@"19.40323",@"19.458006",@"19.416085",@"19.44551",@"19.459913",@"19.304334",@"19.427519",@"19.43846",
                    
                    @"19.314604",@"19.539192",@"19.352388",@"19.504675",@"19.65057",@"19.503237",@"19.52153",@"19.393967",@"19.607572",@"19.40926",@"19.530821",@"19.609727",@"19.635042",@"19.606836",@"19.58159",@"19.557397",@"19.583028",@"19.310351",
                    @"23.002276",@"23.151352",@"22.895770",@"23.086853",@"22.904792",@"24.105281",@"24.153159",@"24.135771",@"22.935531",@"16.23469",@"31.697420",@"31.659190",@"29.321015",@"25.455622",@"25.534774",@"25.557605",@"25.564479",@"25.464519",@"25.434526",@"24.042104",@"24.030698",@"21.140686",@"20.531425",@"20.562719",@"16.843548",@"20.098007",@"20.058305",@"19.831158",@"20.091299",@"20.677073",@"20.675126",@"18.802595",@"18.850324",@"18.894956",@"18.858872",@"18.796463",@"18.850280",@"18.817839",@"25.565807",@"25.683052",@"25.661864",@"20.137692",@"19.071427",@"22.140688",@"24.790420",@"24.748455",@"24.768341",@"17.971260",@"17.970406",@"17.998588",@"18.274346",@"18.069121",@"26.076051",@"27.457538",@"22.297527",@"26.086235",@"22.742264",@"22.395597",@"25.877302",@"19.317274",@"19.551121",@"19.551343",@"20.543039",@"19.161713",@"18.143298",@"17.994762",@"18.861788",
                    ////
                    @"19.310121",@"19.288649",@"19.317792",
                    @"19.471197",@"19.407975",@"19.357566",@"19.613325",@"19.544622",@"19.533884",@"19.604569",@"19.423684",@"19.427142",
                    
                    @"19.542714",@"19.287092",@"20.069003",@"19.620142",@"19.302670",@"18.756362",@"19.672277",@"19.559532",@"19.371266",@"19.255351",
                    
                    @"21.909224",@"20.652613",@"21.037230",@"19.178809",@"19.688645",@"18.882882",@"14.904024",@"20.095098",@"20.094088",@"19.089187",
                    @"22.442362",@"22.443174",@"19.585189",@"19.464698",@"19.375722",@"19.14104",@"20.64294",
                    
                    nil];
    self.latitudeArray = lat;
    
    
    NSArray *log = [[NSArray alloc]initWithObjects:
                    @"-99.20505",@"-99.187755",@"-99.138714",@"-99.14225",@"-99.144552",@"-99.188213",@"-99.165591",@"-99.167293",@"-99.170389",@"-99.138434",@"-99.150643",@"-99.171627",@"-99.163799",@"-99.15476",@"-99.138691",@"-99.1699",@"-99.161963",@"-99.167772",@"-99.146083",@"-99.161184",@"-99.128752",@"-99.176316",@"-99.155957",@"-99.165733",@"-99.170339",@"-99.159623",@"-99.155203",@"-99.167945",@"-99.129459",@"-99.118939",@"-99.121399",@"-99.132494",@"-99.150969",@"-99.124335",@"-99.099316",@"-99.132601",@"-99.125331",@"-98.994524",@"-99.099048",@"-99.019337",@"-99.080967",@"-99.033414",@"-99.037785",@"-99.175644",@"-99.207251",@"-99.183052",@"-99.200356",@"-99.18245",@"-99.201309",@"-99.187037",@"-99.060239",@"-99.119989",@"-99.119214",
                    
                    @"-98.882173",@"-99.224165",@"-98.667119",@"-99.040053",@"-99.183479",@"-99.212275",@"-99.092722",@"-98.972088",@"-98.973499",@"-98.927092",@"-99.049599",@"-99.018338",@"-99.114406",@"-99.051557",@"-99.203241",@"-99.204432",@"-99.041281",@"-98.947479",
                    @"-109.733004",@"-109.709030",@"-109.916131",@"-109.707037",@"-109.938209",@"-110.311908",@"-110.324586",@"-110.332314",@"-109.931061",@"-93.897614",@"-106.411211",@"-106.440492",@"-100.956930",@"-101.009722",@"-103.394909",@"-103.436258",@"-103.443017",@"-100.978909",@"-100.999369",@"-104.629825",@"-104.642506",@"-101.633373",@"-100.836291",@"-101.198016",@"-99.911668",@"-98.761428",@"-99.334672",@"-98.979140",@"-98.754250",@"-103.353537",@"-103.357288",@"-99.197827",@"-99.178932",@"-99.166260",@"-98.959235",@"-98.916441",@"-98.935401",@"-98.918962",@"-100.239510",@"-100.485246",@"-100.152467",@"-98.097994",@"-98.193666",@"-100.955913",@"-107.425275",@"-107.426313",@"-107.370869",@"-92.923560",@"-92.971035",@"-93.386761",@"-93.218774",@"-93.164282",@"-98.315527",@"-99.511043",@"-97.876811",@"-98.301626",@"-98.962800",@"-97.931399",@"-97.521629",@"-98.221853",@"-98.494730",@"-98.493660",@"-97.467543",@"-96.108613",@"-94.473048",@"-94.541216",@"-97.108012",
                    ///////
                    @"-99.561357",@"-99.688153",@"-99.630311",
                    
                    @"-99.223602",@"-98.924826",@"-98.986859",@"-99.186181",@"-99.211134",@"-99.219766",@"-99.189786",@"-99.152787",@"-99.139239",@"-99.233329",
                    
                    @"-99.557156",@"-99.220842",@"-99.211247",@"-99.627937",@"-99.242948",@"-99.226320",@"-99.047754",@"-99.080088",@"-99.619521",@"-102.317655",
                    
                    @"-100.472870",@"-104.286406",@"-96.187986",@"-99.226264",@"-96.922567",@"-92.247495",@"-99.29945",@"-99.299664",@"-96.153463",@"-105.416328",
                    
                    @"-105.415668",@"-99.20282",@"-99.218909",@"-99.097216",@"-96.116363",@"-100.459561",
                    nil];
    
    self.longitudeArray = log;
    

    NSLog(@"adress: %d",[adressArray count]);
    NSString *adrsText = [[NSString alloc]initWithFormat:@"%@", [self.adressArray objectAtIndex:[detCount intValue]]];
    adrsView.text = adrsText;
    ttLabel.text = titl;

       
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)pushDemo{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"DEMO Desarrollado por ELO" message:@"Esta opción aun no esta activada." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
    // [alert release];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)goToMaps{
    
 //   NSString* address = [NSString stringWithFormat:@"%@", [adressArray objectAtIndex:[detCount intValue]]];
     NSString* dlat = [NSString stringWithFormat:@"%@", [latitudeArray objectAtIndex:[detCount intValue]]];
     NSString* dlong = [NSString stringWithFormat:@"%@", [longitudeArray objectAtIndex:[detCount intValue]]];
    //NSString* url = [NSString stringWithFormat: @"http://maps.apple.com/maps?saddr=%f,%f&daddr=%@",
                    // [latST floatValue], [longST floatValue],
                //     [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString* url = [NSString stringWithFormat: @"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f",
                     [latST floatValue], [longST floatValue],[dlat floatValue], [dlong floatValue] ];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    
    
}

-(IBAction)gotoBack:(id)sender
{
       UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, -62, 320, 480))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ///////
    [self.navigationController popViewControllerAnimated:YES];
}

///////////////////
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (orientationchangefunction:) name:UIDeviceOrientationDidChangeNotification object:nil];
    [self orientationchngfn];
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        orientseason=0;
    }
    if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        orientseason=1;
    }
    if(interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        orientseason=1;
    }
    if(interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        orientseason=0;
    }
    if(orientseason==0)
    {
    }
    else if(orientseason==1)
    {
        
    }
    
    return YES;
}

-(void) orientationchangefunction:(NSNotification *) notificationobj
{
    
    [self performSelector:@selector(orientationchngfn) withObject:nil afterDelay:0.0];
}
-(void) orientationchngfn
{
    UIDeviceOrientation dorientation =[UIDevice currentDevice].orientation;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        if(UIDeviceOrientationIsPortrait(dorientation))
            
        {
            orientseason=0;
        }
        else if (UIDeviceOrientationIsLandscape(dorientation))
        {
            orientseason=1;
        }
        if(orientseason==0)
        {
           // mapView.frame=CGRectMake(0, 0, 768, 1004);
            
            
        }
        else if(orientseason==1)
        {
           // mapView.frame=CGRectMake(0, 0, 1004, 768);
            
        }
        
    }
    else {
        if(UIDeviceOrientationIsPortrait(dorientation))
            
        {
            orientseason=0;
        }
        else if (UIDeviceOrientationIsLandscape(dorientation))
        {
            
            orientseason=1;
        }
        if(orientseason==0)
        {
           
            
          //  serviviosView.frame = CGRectMake(270, 700, 395, 180);
            UIGraphicsBeginImageContext(self.view.frame.size);
            [[UIImage imageNamed:@"Fondo_iPad.png"] drawInRect:(CGRectMake(0, 0, 768, 1004))];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];

              titulo.frame = CGRectMake(288, 458, 239, 47);
            
                  tienda.frame = CGRectMake(581, 544, 90, 90);
              tarjeta.frame = CGRectMake(362, 544, 90, 90);
              atm.frame = CGRectMake(121, 544, 90, 90);
            
                      
        }
        else if(orientseason==1)
        {
            
            UIGraphicsBeginImageContext(CGSizeMake(1024, 768));
            [[UIImage imageNamed:@"Fondo-landscape-2008-x1536.png"] drawInRect:(CGRectMake(0, 0, 1024, 768))];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];

            
            
            titulo.frame = CGRectMake(50, 500, 239, 47);
            tarjeta.frame = CGRectMake(240, 550, 90, 90);
             atm.frame = CGRectMake(50, 550, 90, 90);
             tienda.frame = CGRectMake(140, 600, 90, 90);
            
                      
                       NSLog(@"TURN!!!");
            
            
        }
    }
    
}





@end
