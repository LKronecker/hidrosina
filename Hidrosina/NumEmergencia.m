//
//  NumEmergencia.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-14.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "NumEmergencia.h"
#define IS_WIDESCREEN ( fabs (( double ) [[ UIScreen mainScreen ] bounds].size.height - (double )568 ) < DBL_EPSILON)
@interface NumEmergencia ()

@end

@implementation NumEmergencia

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 85, 35);
    //[button setTitle:@"Regresar" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"BOTÓN-REGRESAR.png"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    //////
    if (IS_WIDESCREEN) {
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageNamed:@"fondo-números-de-emergencia.png"] drawInRect:(CGRectMake(0, 0, 320, 509))];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(llamar)
          forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"Bóton-Angeles-verdes.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(2, 160, 165, 88);
        
        [self.view addSubview:button1];
        ////
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button2 addTarget:self
                    action:@selector(llamar1)
          forControlEvents:UIControlEventTouchDown];
        
        [button2 setBackgroundImage:[UIImage imageNamed:@"Bóton-Ambulancia.png"] forState:UIControlStateNormal];
        
        button2.frame = CGRectMake(154, 160, 165, 88);
        
        [self.view addSubview:button2];
        /////
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button3 addTarget:self
                    action:@selector(llamar2)
          forControlEvents:UIControlEventTouchDown];
        
        [button3 setBackgroundImage:[UIImage imageNamed:@"Bóton-Bomberos.png"] forState:UIControlStateNormal];
        
        button3.frame = CGRectMake(2, 270, 165, 88);
        
        [self.view addSubview:button3];
        ////
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button4 addTarget:self
                    action:@selector(llamar3)
          forControlEvents:UIControlEventTouchDown];
        
        [button4 setBackgroundImage:[UIImage imageNamed:@"Bóton-Policia.png"] forState:UIControlStateNormal];
        
        button4.frame = CGRectMake(154, 270, 165, 88);
        
        [self.view addSubview:button4];
        ////
        UIButton *button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button5 addTarget:self
                    action:@selector(llamar4)
          forControlEvents:UIControlEventTouchDown];
        
        [button5 setBackgroundImage:[UIImage imageNamed:@"Bóton-Auxilio-vial.png"] forState:UIControlStateNormal];
        
        button5.frame = CGRectMake(2, 390, 165, 88);
        
        [self.view addSubview:button5];
        //////////
        UIButton *button6 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button6 addTarget:self
                    action:@selector(llamar5)
          forControlEvents:UIControlEventTouchDown];
        
        [button6 setBackgroundImage:[UIImage imageNamed:@"Bóton-Protección-civil2.png"] forState:UIControlStateNormal];
        
        button6.frame = CGRectMake(154, 390, 165, 88);
        
        [self.view addSubview:button6];
    }else{
       
    /////
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(llamar)
          forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"Bóton-Angeles-verdes.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(2, 85, 165, 88);
        
        [self.view addSubview:button1];
////
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button2 addTarget:self
                    action:@selector(llamar1)
          forControlEvents:UIControlEventTouchDown];
        
        [button2 setBackgroundImage:[UIImage imageNamed:@"Bóton-Ambulancia.png"] forState:UIControlStateNormal];
        
        button2.frame = CGRectMake(154, 85, 165, 88);
        
        [self.view addSubview:button2];
/////
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button3 addTarget:self
                    action:@selector(llamar2)
          forControlEvents:UIControlEventTouchDown];
        
        [button3 setBackgroundImage:[UIImage imageNamed:@"Bóton-Bomberos.png"] forState:UIControlStateNormal];
        
        button3.frame = CGRectMake(2, 187, 165, 88);
        
        [self.view addSubview:button3];
////
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button4 addTarget:self
                    action:@selector(llamar3)
          forControlEvents:UIControlEventTouchDown];
        
        [button4 setBackgroundImage:[UIImage imageNamed:@"Bóton-Policia.png"] forState:UIControlStateNormal];
        
        button4.frame = CGRectMake(154, 187, 165, 88);
        
        [self.view addSubview:button4];
////
        UIButton *button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button5 addTarget:self
                    action:@selector(llamar4)
          forControlEvents:UIControlEventTouchDown];
        
        [button5 setBackgroundImage:[UIImage imageNamed:@"Bóton-Auxilio-vial.png"] forState:UIControlStateNormal];
        
        button5.frame = CGRectMake(2, 294, 165, 88);
        
        [self.view addSubview:button5];
        //////////
        UIButton *button6 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button6 addTarget:self
                    action:@selector(llamar5)
          forControlEvents:UIControlEventTouchDown];
        
        [button6 setBackgroundImage:[UIImage imageNamed:@"Bóton-Protección-civil2.png"] forState:UIControlStateNormal];
        
        button6.frame = CGRectMake(154, 294, 165, 88);
        
        [self.view addSubview:button6];
//////

    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-números-de-emergencia.png"] drawInRect:(CGRectMake(0, 0, 320, 420))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)llamar{
    NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:078"];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];

}
-(IBAction)llamar1{
    NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:065"];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
}
-(IBAction)llamar2{
    NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:57682539"];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
}
-(IBAction)llamar3{
    NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:066"];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
}
-(IBAction)llamar4{
    NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:072"];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
}
-(IBAction)llamar5{ NSURL *phoneNumberURL = [NSURL URLWithString:@"tel:56832222"];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
}

-(IBAction)gotoBack:(id)sender
{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-hidro-registro.png"] drawInRect:(CGRectMake(0, -62, 320, 480))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ///////
    [self.navigationController popViewControllerAnimated:YES];
}

@end
