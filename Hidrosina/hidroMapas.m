//
//  hidroMapas.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "hidroMapas.h"
#import "HSPlaces.h"



@interface hidroMapas ()

@property (nonatomic) HSPlaces * places;
//@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *userLocation;

@end

@implementation hidroMapas
@synthesize mapView, latitudeArray, longitudeArray, tittleArray, subtArray, gdet, control, gasArray, RangeControl, km1Button, km2Button, km3Button, home, webView, activityindicator, webView1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)changeSeg:(id)sender{
	
	if (control.selectedSegmentIndex == 0) {
		mapView.mapType = MKMapTypeStandard;
	}
	if (control.selectedSegmentIndex == 1) {
		mapView.mapType = MKMapTypeSatellite;
	}
	if (control.selectedSegmentIndex == 2) {
		mapView.mapType = MKMapTypeHybrid;
	}
    
}

-(IBAction)changeRange:(id)sender{
    
    if (control.selectedSegmentIndex == 0) {
		[self gotoLocation];
	}
	if (control.selectedSegmentIndex == 1) {
		[self gotoLocation1];
	}
	if (control.selectedSegmentIndex == 2) {
		[self gotoLocation2];
	}


}

- (void)viewDidLoad
{
     [super viewDidLoad];
    
    webView.alpha = 0.45;
    
   
    webView.delegate = self;
    
   
    [webView addSubview:activityindicator];
    
    NSString *urlAddress = @"http://www.hidrosina.com.mx/new/mobile/images/gasolina/gasolina.gif";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    [webView1 loadRequest:requestObj];
    [NSTimer scheduledTimerWithTimeInterval:(1.0/2.0) target:self selector:@selector(loading) userInfo:nil repeats:YES];
    
    [self.view addSubview:webView];
    [self.view bringSubviewToFront:km1Button];
    
 //   [webView sendSubviewToBack:km1Button];
    ///////
    km2Button.selected = YES;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 85, 35);
    //[button setTitle:@"Regresar" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"BOTÓN-REGRESAR.png"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;

    ///////////////////
    [NSTimer scheduledTimerWithTimeInterval:4.5 target:self selector:@selector(places:) userInfo:nil repeats:NO];
    ///////////////
    
    [self initiLocationManager];
    [self setupMap];
}

- (void) setupMap {
    
    mapView = [[MKMapView alloc]initWithFrame:self.view.bounds];
    [self.view insertSubview:mapView atIndex:0];
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    
    [mapView setDelegate:self];
    
    mapView.showsUserLocation = YES;
    
    
}

-(void)initiLocationManager {
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [_locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *userLoc = [locations firstObject];
    self.userLocation = userLoc;
    
}


-(void)loading{
    if(!webView.loading){
        [activityindicator stopAnimating];
        activityindicator.hidden = YES;}
	
    else{
		[activityindicator startAnimating];
        activityindicator.hidden = NO; }
}


-(IBAction)places:(id)sender{
    
    self.places = [[HSPlaces alloc]init];

    self.tittleArray = [self.places tittles];
    self.gasArray = [self.places gasStations];
    self.latitudeArray = [self.places latitudes];
    self.longitudeArray = [self.places longitudes];
    self.subtArray = [self.places subtittles];
    

    
    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
    //  NSString *GPSstring = [[NSString alloc]initWithFormat:@"GPS: %f, %f", userCoordinate.latitude, userCoordinate.longitude];
    //GPSlabel.text = GPSstring;
	
	NSLog(@"user latitude = %f",userCoordinate.latitude);
	NSLog(@"user longitude = %f",userCoordinate.longitude);
	
	mapView.delegate=self;
    
	NSMutableArray* annotations=[[NSMutableArray alloc] init];
    
    for (int i=0; i<[self.tittleArray count]; i++) {
        
        double lon = [[longitudeArray objectAtIndex:i] doubleValue];
        double lat = [[latitudeArray objectAtIndex:i] doubleValue];
        
        NSString *tittleString = [[NSString alloc]initWithFormat:@"%@", [tittleArray objectAtIndex:i]];
        NSString *subttlString = [[NSString alloc]initWithFormat:@"%@",[subtArray objectAtIndex:i]];
                                  
        //:@"%f, %f",[[latitudeArray objectAtIndex:i] doubleValue], [[longitudeArray objectAtIndex:i] doubleValue]];
        
        CLLocationCoordinate2D theCoordinate;
        theCoordinate.latitude = lat;
        theCoordinate.longitude = lon;
        
        MapAnotations* myAnnotation1=[[MapAnotations alloc] init];
        
        myAnnotation1.coordinate=theCoordinate;
        myAnnotation1.title = tittleString;
        myAnnotation1.subtitle = subttlString;
        myAnnotation1.tag = [NSString stringWithFormat:@"%i", i];
        //  myAnnotation1.pinColor = MKPinAnnotationColorPurple;
        
        [mapView addAnnotation:myAnnotation1];
        [annotations addObject:myAnnotation1];
        
    }
    
	
	NSLog(@"annotations: %lu",(unsigned long)[annotations count]);
    NSLog(@"lat: %lu",(unsigned long)[latitudeArray count]);
    NSLog(@"long: %lu",(unsigned long)[longitudeArray count]);
    NSLog(@"tittle: %lu",(unsigned long)[tittleArray count]);
    NSLog(@"subtl: %lu",(unsigned long)[subtArray count]);
    NSLog(@"gas: %lu",(unsigned long)[gasArray count]);
    
    [self gotoLocation1];
    
    

}

- (void)gotoLocation
{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.01f;
    newRegion.span.longitudeDelta = 0.01f;
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
      //  [self.mapView reloadInputViews];
}
- (void)gotoLocation1 {
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.05f;
    newRegion.span.longitudeDelta = 0.05;
	
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
       // [self.mapView reloadInputViews];
}
- (void)gotoLocation2
{

    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.1f;
    newRegion.span.longitudeDelta = 0.1f;
	
    [self.mapView setRegion:newRegion animated:YES];
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    //[self.mapView reloadInputViews];
}

-(IBAction)goLoc:(id)sender{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.01f;
    newRegion.span.longitudeDelta = 0.01f;
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    km1Button.selected = YES;
    km2Button.selected = NO;
    km3Button.selected = NO;
}
-(IBAction)goLoc1:(id)sender{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.05f;
    newRegion.span.longitudeDelta = 0.05;
	
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    km1Button.selected = NO;
    km2Button.selected = YES;
    km3Button.selected = NO;

}
-(IBAction)goLoc2:(id)sender{
    CLLocationCoordinate2D userCoordinate = self.userLocation.coordinate;
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = userCoordinate.latitude;
    newRegion.center.longitude = userCoordinate.longitude;
    newRegion.span.latitudeDelta = 0.1f;
    newRegion.span.longitudeDelta = 0.1f;
	
    [self.mapView setRegion:newRegion animated:YES];
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:newRegion];
	
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    km1Button.selected = NO;
    km2Button.selected = NO;
    km3Button.selected = YES;
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)gotoBack:(id)sender
{
    UIGraphicsBeginImageContext (CGSizeMake(320, 100));
    [[UIImage imageNamed:@"nab-bar2.png"] drawInRect:(CGRectMake(0, 0, 320, 100))];
    UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    
    //////////
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-nab-bar2.png"] drawInRect:(CGRectMake(0, -62, 320, 480))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ///////
    [self.navigationController popViewControllerAnimated:YES];
   // HidrosinaHome *mp = [[HidrosinaHome alloc]initWithNibName:@"Facturacion" bundle:[NSBundle mainBundle]];
    
  //  self.home = mp;
    //[self.navigationController pushViewController:mp animated:YES];
   
}


#pragma mark MKMapViewDelegate
/*
 - (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
 {
 return [kml viewForOverlay:overlay];
 }
 */
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	//NSLog(@"welcome into the map view annotation");
	
	// if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	// try to dequeue an existing pin view first
	static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
	MKPinAnnotationView* pinView = [[MKPinAnnotationView alloc]
									 initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
    
	  pinView.image = [UIImage imageNamed:@"pin.png"];
    pinView.animatesDrop=YES;
	pinView.canShowCallout=YES;
    pinView.pinColor=MKPinAnnotationColorGreen;
  //  annotation.t
   
    
	
	
	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	[rightButton setTitle:annotation.title forState:UIControlStateNormal];
	[rightButton addTarget:self
					action:@selector(showDetails:)
		  forControlEvents:UIControlEventTouchUpInside];
	pinView.rightCalloutAccessoryView = rightButton;
	
	UIImageView *profileIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin-1.png"]];
	pinView.leftCalloutAccessoryView = profileIconView;
	//[profileIconView release];
	
	
	return pinView;
}

-(IBAction)pushDemo{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"DEMO Desarrollado por ELO" message:@"Esta opción aun no esta activada." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
   // [alert release];
    
}


-(IBAction)showDetails:(id)sender{
    
	//NSLog(@"Annotation Click");
    
    //To be safe, may want to check that array has at least one item first.
    
 //   id<MKAnnotation> ann = [[mapView selectedAnnotations] objectAtIndex:0];
    
    // OR if you have custom annotation class with other properties...
    // (in this case may also want to check class of object first)
    
    MapAnotations *an = [[mapView selectedAnnotations] objectAtIndex:0];
    
    NSLog(@"ann.title = %@", an.title);
    //  NSLog(@"ann.coord = %@", an.coordinate);
    
    ////////
    CLLocation *userLoc = mapView.userLocation.location;
    CLLocationCoordinate2D userCoordinate = userLoc.coordinate;
    
        
    NSString *imST = [[NSString alloc]initWithFormat:@"%@",an.title];
    int index = [tittleArray indexOfObject:imST];
    
    GasDetails *det = [[GasDetails alloc]initWithNibName:@"GasDetails" bundle:[NSBundle mainBundle]];

    
    NSString *laST = [NSString stringWithFormat:@"%f", userCoordinate.latitude];
    NSString *loST = [NSString stringWithFormat:@"%f", userCoordinate.longitude];
    NSString *gST = [NSString stringWithFormat:@"%@", [gasArray objectAtIndex:index]];
    
    det.longST = loST;
    det.latST = laST;
    
    det.gasST = gST;
    
    NSLog(@"%@", gST);
    
    det.diesel.hidden = YES;

    
    
    
    det.titl = an.title;
       
    NSString *contSt = [[NSString alloc]initWithFormat:@"%i",index];
    det.detCount = contSt;
    NSLog(@"%i, %@",index, contSt);
    
    
    self.gdet = det;
    [self.navigationController pushViewController:det animated:YES];
}

-(void)webView:(UIWebView *)webview didFailLoadWithError:(NSError *)error {
    
    // if (error.code == NSURLErrorNotConnectedToInternet){
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Imposible descargar contenido, verifique su conexión a Internet."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    NSLog(@"Alert");
    
    webView.alpha = 0;
    
}


@end
