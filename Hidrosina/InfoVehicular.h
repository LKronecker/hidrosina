//
//  InfoVehicular.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-14.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebController.h"

@class WebController;




@interface InfoVehicular : UIViewController <UIPickerViewDelegate>{

    UITextField *myTextField;
    int seleCount;
        
    UIButton * poliza;
        UIButton * marca;
        UIButton * modelo;
        UIButton * ano;
     UIButton * aseguradora;
    UIButton * llamar;
    
    bool pickerCount;

    NSArray *brandArray;
    NSArray *seguroArray;
     NSArray *urlSegArray;
    NSArray *urlArray;
    NSArray *numArray;
    
    UIPickerView *brandPicker;
     UIPickerView *segPicker;
    UIView *pickerView;
   
    WebController *webCont;
}

@property (nonatomic, retain)  WebController *webCont;
@property (nonatomic, retain) UITextField *myTextField;
@property (nonatomic, retain) IBOutlet UIButton * poliza;
@property (nonatomic, retain) IBOutlet UIButton * marca;
@property (nonatomic, retain) IBOutlet UIButton * modelo;
@property (nonatomic, retain) IBOutlet UIButton * ano;
@property (nonatomic, retain) IBOutlet UIButton * llamar;
    @property (nonatomic, retain) IBOutlet UIButton * aseguradora;
 
 @property (nonatomic, retain) IBOutlet    UIPickerView *brandPicker;
  @property (nonatomic, retain) IBOutlet      UIPickerView *segPicker;
@property (nonatomic, retain) IBOutlet  UIView *pickerView;
@property (nonatomic, retain)  NSArray *seguroArray;
@property (nonatomic, retain)  NSArray *urlSegArray;
  @property (nonatomic, retain)  NSArray *numArray;
@property (nonatomic, retain) NSArray *urlArray;
@property (nonatomic, retain)  NSArray *brandArray;


-(IBAction)gotoBack:(id)sender;
-(IBAction)pushDemo:(id)sender;
-(IBAction)pushDemo1:(id)sender;
-(IBAction)pushDemo2:(id)sender;
-(IBAction)pushDemo3:(id)sender;
-(IBAction)elegirMarca:(id)sender;
-(IBAction)configV:(id)sender;
-(IBAction)pushSeguro:(id)sender;

-(IBAction)pushWeb:(id)sender;
-(IBAction)cancel;
-(IBAction)call:(id)sender;




@end
