//
//  HSStaticDataDelegate.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-28.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol HSStaticDataDelegate <NSObject>

@required
- (void)didStartFetching;
- (void)didEndFetchingWith:(NSArray*)model;
- (void)didEndFetchingWithError:(NSError*)error;
- (BOOL)supportModelType:(Class)modelClassType;

@end
