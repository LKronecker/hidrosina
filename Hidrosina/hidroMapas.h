//
//  hidroMapas.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-07.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapAnotations.h"
#import "GasDetails.h"
#import "HidrosinaHome.h"

@class GasDetails, HidrosinaHome;

@interface hidroMapas : UIViewController <MKMapViewDelegate, UIWebViewDelegate, CLLocationManagerDelegate>{
    MKMapView *mapView;
    GasDetails *gdet;
    HidrosinaHome *home;
    NSArray *gasArray;
    NSArray *subtArray;
    NSArray *latitudeArray;
    NSArray *longitudeArray;
    NSArray *tittleArray;
    UISegmentedControl *control;
    
     UISegmentedControl *RangeControl;
    
    UIButton *km1Button;
    UIButton *km2Button;
    UIButton *km3Button;
    
    UIWebView *webView;
    UIWebView *webView1;
    UIActivityIndicatorView *activityindicator;
    
}
@property (nonatomic, retain) IBOutlet UISegmentedControl *control;
  @property (nonatomic, retain) IBOutlet UISegmentedControl *RangeControl;
@property (nonatomic, retain) IBOutlet UIButton *km1Button;
@property (nonatomic, retain) IBOutlet UIButton *km2Button;
@property (nonatomic, retain) IBOutlet UIButton *km3Button;
@property (nonatomic, retain)  NSArray *latitudeArray;
@property (nonatomic, retain)  NSArray *longitudeArray;
@property (nonatomic, retain)  NSArray *tittleArray;
 @property (nonatomic, retain)   NSArray *gasArray;
@property (nonatomic, retain) MKMapView *mapView;
@property (nonatomic, retain) NSArray *subtArray;
 @property (nonatomic, retain)   GasDetails *gdet;
  @property (nonatomic, retain)  HidrosinaHome *home;

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIWebView *webView1;
@property (nonatomic, retain) IBOutlet     UIActivityIndicatorView *activityindicator;

-(IBAction)places:(id)sender;
-(IBAction)showDetails:(id)sender;
-(IBAction)changeSeg:(id)sender;
-(IBAction)pushDemo;
-(IBAction)changeRange:(id)sender;

-(IBAction)goLoc:(id)sender;
-(IBAction)goLoc1:(id)sender;
-(IBAction)goLoc2:(id)sender;
@end
