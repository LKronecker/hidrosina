//
//  HSDataStoreDelegate.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-28.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Goal of this class is to define a protocol for interacting with SWDatastore. This interface will keep all registered controllers updated on ongoing data operations. <br /> */
@protocol HSDataStoreDelegate <NSObject>

@required
- (void)didStartFetching;
- (void)didEndFetchingWithSuccess:(NSArray *)data;
- (void)didEndFetchingWithError:(NSError *)error;

/// -----------
/// @name Supported Model Types
/// -----------

/** Delegates must implement this to let SWDatastore know which types of data (as defined by specific Model Classes ie SWWidget, YPGDeal, etc) they are interested in receiving updates for
 @param the Class of the data for which loading operations are ongoing (ie SWWidget)
 @return a boolean indicating if the delegate is interested in the type of data passed in the param
 */
- (BOOL)supportModelType:(Class)modelClassType;
