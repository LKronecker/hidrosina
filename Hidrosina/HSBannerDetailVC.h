//
//  HSBannerDetailVC.h
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-27.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSBannerDetailVC : UIViewController <UIScrollViewDelegate, UIAlertViewDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *bannerImageView;

@property (nonatomic, retain)IBOutlet UIButton *butonRegresar;

@property (nonatomic, retain)IBOutlet UIButton *masInformacionButton;

@property (nonatomic, retain)IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain)  NSString *urlString;

-(IBAction)regresar;
- (id)initWithImage: (NSString*)image andURL:(NSString *)url;
-(IBAction)masInformacion;
- (IBAction)openDaleDietrichDotCom:(id)sender;
@end
