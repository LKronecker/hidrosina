//
//  HSBannerCollectionViewCell.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-27.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import "HSBannerCollectionViewCell.h"



@implementation HSBannerCollectionViewCell

//- (void)awakeFromNib {
//    // Initialization code
//    NSArray *array =[[NSArray alloc]initWithObjects:@"0.png",@"1.png",@"2.png",@"3.png",@"4.png", nil];
//    self.bannerImagesArray = [[NSMutableArray alloc]init];
//    
//    for (int i = 0; i<5; i++) {
//        //  NSString *imgStr = [NSString stringWithFormat:@"%i.png"];
//       // [self.bannerImagesArray addObject:[self downloadImage:[array objectAtIndex:i]]];
//        [self downloadImage:[array objectAtIndex:i]];
//    }
//}
//
//- (void)downloadImage:(NSString *)image;
//{
//    //    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
//    //    [activity setCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];
//    //    [self addSubview:activity];
//    //    [activity startAnimating];
//    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://i1065.photobucket.com/albums/u395/lkronecker/%@",image]];
//    //   http://i1065.photobucket.com/albums/u395/lkronecker/0F.png
//    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
//    dispatch_async(download_queue, ^{
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        UIImage *_image = [UIImage imageWithData:data];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            //   [activity stopAnimating];
//            //   [activity removeFromSuperview];
//            _bannerImage = _image;
//            [self.bannerImagesArray addObject:_image];
//            
//        });
//    });
//    // dispatch_release(download_queue);
//   // return _bannerImage;
//}
//
//-(void)setImage:(NSInteger)imageNum{
//    
//    if ([self.bannerImagesArray objectAtIndex:0]) {
//    
//    [self.bannerImageView setImage:[self.bannerImagesArray objectAtIndex:imageNum ]];
//    }
//}
//
//
- (void)awakeFromNib {
    // Initialization code
    
    self.layer.cornerRadius = 17.0f;
}

- (void)downloadImage:(NSString *)image;
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];
    [self addSubview:activity];
   // if (self.bannerImageView.image == nil)
        [activity startAnimating];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://i1065.photobucket.com/albums/u395/lkronecker/%@",image]];
    //   http://i1065.photobucket.com/albums/u395/lkronecker/0F.png
    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
    dispatch_async(download_queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *_image = [UIImage imageWithData:data];

        dispatch_async(dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            [activity removeFromSuperview];
            [self.bannerImageView setImage:_image];
            
        });
    });
    // dispatch_release(download_queue);
}


@end
