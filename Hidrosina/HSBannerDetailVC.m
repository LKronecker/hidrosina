//
//  HSBannerDetailVC.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2014-10-27.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import "HSBannerDetailVC.h"
#import "WebController.h"

@interface HSBannerDetailVC ()

@property (retain) NSArray *urlArray;

@end

@implementation HSBannerDetailVC

- (id)initWithImage: (NSString*)image andURL:(NSString *)url{
    if (self) {
        
        [self downloadImage:image];
        self.urlString = url;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.maximumZoomScale = 2.0;
    
    NSArray *array = [[NSArray alloc]initWithObjects:@"http://clubpremier.com/mx/gana-kilometros/tiendas-y-mas/gasolineras/hidrosina", @"", @"http://teleton.org/", @"", @"", nil];
    self.urlArray =array;
    
    if ([self.urlString isEqualToString:@"2"])  self.masInformacionButton.hidden = YES;
            if ([self.urlString isEqualToString:@"3"])self.masInformacionButton.hidden = YES;
                    if ([self.urlString isEqualToString:@"5"])self.masInformacionButton.hidden = YES;
    
   // for (int i = 0; i<[array count]; i ++) {
    //    if ([[array objectAtIndex:i] isEqualToString:@""]) {
     //       self.masInformacionButton.hidden = YES;
     //   }
//    }
  //  self.bannerImageView.
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)regresar{

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)masInformacion{
    NSUInteger dude = [self.urlString integerValue] ;
    if (dude == 2 || dude == 0) {
    NSURL *url = [[NSURL alloc]initWithString:[self.urlArray objectAtIndex:[self.urlString integerValue]]];
    [[UIApplication sharedApplication] openURL:url];
    }
    else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:nil message:@"No hay mas información disponible por el momento." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }

}

- (void)downloadImage:(NSString *)image;
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2)];
    [self.view addSubview:activity];
    [activity startAnimating];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://i1065.photobucket.com/albums/u395/lkronecker/%@",image]];
    //   http://i1065.photobucket.com/albums/u395/lkronecker/0F.png
    dispatch_queue_t download_queue = dispatch_queue_create("download_file", NULL);
    dispatch_async(download_queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *_image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            [activity removeFromSuperview];
            [self.bannerImageView setImage:_image];
            
        });
    });
    // dispatch_release(download_queue);
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{

    return self.bannerImageView;
}

- (IBAction)openDaleDietrichDotCom:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.daledietrich.com"]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
