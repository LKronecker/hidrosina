//
//  InfoVehicular.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-02-14.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "InfoVehicular.h"
#define IS_WIDESCREEN ( fabs (( double ) [[ UIScreen mainScreen ] bounds].size.height - (double )568 ) < DBL_EPSILON)
@interface InfoVehicular ()

@end

@implementation InfoVehicular
@synthesize myTextField, poliza, marca, modelo, ano, brandArray, brandPicker, pickerView, webCont, urlArray, aseguradora, seguroArray, urlSegArray, segPicker,numArray, llamar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    llamar.hidden = YES;
    
    UIGraphicsBeginImageContext(self.pickerView.frame.size);
    [[UIImage imageNamed:@"fondo-información-vehicular.png"] drawInRect:(CGRectMake(0, 0, 320, 509))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.pickerView.backgroundColor = [UIColor colorWithPatternImage:image];
    pickerCount = 0;
    pickerView.alpha =  0;
    
    NSArray *phones = [[NSArray alloc]initWithObjects:@"018007122828",@"018000011300",@"018001111200",@"018009001292",@"018007093800",@"018004009000",@"018000115900",@"54903939",@"018008002021",@"91775000",@"018008327696",@"018002266783",@"018001122800",@"018009090000",nil];
    self.numArray = phones;
    
   
    
    NSArray *segs = [[NSArray alloc]initWithObjects:@"ABA Seguros",@"AIG M'exico",@"Allianx M'exico",@"AXA",@"El aguila",@"GNP",@"Grupo Zurich",@"Mapfre",@"Qualitas",@"Seguros Atlas",@"Seguros Banamex",@"Seguros Banorte",@"Seguros BBVA Bancomer",@"Seguros Inbursa"
, nil];
    self.seguroArray = segs;
    
    NSArray *urlSeg = [[NSArray alloc]initWithObjects:@"http://www.abaseguros.com/Paginas/default.aspx",@"http://www.aigmex.com",@"http://www.allianz.com.mx/home",@"http://axa.mx/Personas/AxaSeguros/default.aspx",@"http://www.elaguila.com.mx",@"https://www.gnp.com.mx",@"http://www.zurich.com.mx",@"http://www.mapfre.com.mx",@"http://www.qualitas.com.mx",@"http://www.segurosatlas.com.mx",@"http://www.segurosbanamex.com.mx",@"http://www.banorte.com/portal/personas/home.web",@"https://www.segurosbancomer.com.mx/seguros/default.asp",@"http://www.inbursa.com/index.asp",@"http://www.mnyl.com.mx", nil];
    self.urlSegArray =urlSeg;
    
       
    NSArray *urls = [[NSArray alloc]initWithObjects:@"http://www.ford.mx",@"http://www.chrysler.com.mx",
                     @"http://www.dodge.com.mx",@"http://www.nissan.com.mx",@"http://www.gmc.com.mx",
                     @"http://www.bmw.com.mx",@"http://www2.mercedes-benz.com.mx",
                   @"http://www.jeep.com.mx",@"http://www.camionesram.com.mx",@"http://www.fiat.com.mx",@"http://www.alfaromeo.mx",@"http://www.mitsubishi-motors.com.mx",@"http://www.acura.mx",@"http://www.honda.mx",@"http://www.mazdamexico.com.mx",@"http://www.toyota.com.mx",@"http://www.vw.com.mx",@"http://www.pontiac.com.mx",@"http://www.peugeot.com.mx",@"http://renault.com.mx",@"http://www.suzuki.com.mx",@"http://www.mini.com.mx",@"http://www.audi.com.mx", nil];
    
    self.urlArray =urls;

    NSArray *brands = [[NSArray alloc]initWithObjects:@"Ford",@"Chrysler",@"Dodge",@"Nissan",@"GMC",@"BMW",@"Mercedes Benz",@"Jeep",@"Ram",@"Fiat",@"Alfa Romeo",@"Mitsubishi", @"Acura",@"Honda",@"Mazda",@"Toyota",@"Volkswagen",@"Pontiac",@"Peugeot",@"Renault",@"Suzuki",@"Mini",@"Audi",  nil];
    
    self.brandArray = brands;
  /*
    [poliza setTitle:[[NSUserDefaults standardUserDefaults]
                      stringForKey:@"poliza"] forState:UIControlStateNormal];
    [marca setTitle:[[NSUserDefaults standardUserDefaults]
                     stringForKey:@"marca"] forState:UIControlStateNormal];
    [modelo setTitle:[[NSUserDefaults standardUserDefaults]
                      stringForKey:@"modelo"] forState:UIControlStateNormal];
    [ano setTitle:[[NSUserDefaults standardUserDefaults]
                   stringForKey:@"ano"] forState:UIControlStateNormal];
    */
    seleCount = 0;
    /////
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(gotoBack:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 85, 35);
    //[button setTitle:@"Regresar" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"BOTÓN-REGRESAR.png"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    /////
    if (IS_WIDESCREEN) {
        UIGraphicsBeginImageContext(self.pickerView.frame.size);
        [[UIImage imageNamed:@"fondo-gasolineriasF.png"] drawInRect:(CGRectMake(0, 0, 320, 500))];
        UIImage *image5 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.pickerView.backgroundColor = [UIColor colorWithPatternImage:image5];
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageNamed:@"fondo-información-vehicular.png"] drawInRect:(CGRectMake(0, 0, 320, 509))];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(pushDemo:)
          forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"Numero-de-pólizaF.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(20.0, 84.0, 280.0, 75.0);
        
        self.poliza = button1;
        [self.view addSubview:button1];
        /////
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button2 addTarget:self
                    action:@selector(pushDemo1:)
          forControlEvents:UIControlEventTouchDown];
        
        [button2 setBackgroundImage:[UIImage imageNamed:@"Marca-del-vehículoF.png"] forState:UIControlStateNormal];
        
        button2.frame = CGRectMake(20.0, 164.0, 280.0, 75.0);
        self.marca = button2;
        [self.view addSubview:button2];
        /////
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button3 addTarget:self
                    action:@selector(pushDemo2:)
          forControlEvents:UIControlEventTouchDown];
        
        [button3 setBackgroundImage:[UIImage imageNamed:@"Modelo-del-vehículoF.png"] forState:UIControlStateNormal];
        
        button3.frame = CGRectMake(20.0, 244.0, 280.0, 75.0);
        self.modelo = button3;
        [self.view addSubview:button3];
        /////
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button4 addTarget:self
                    action:@selector(pushDemo3:)
          forControlEvents:UIControlEventTouchDown];
        
        [button4 setBackgroundImage:[UIImage imageNamed:@"Año-del-vehículoF.png"] forState:UIControlStateNormal];
        
        button4.frame = CGRectMake(20.0, 324.0, 280.0, 75.0);
        self. ano = button4;
        [self.view addSubview:button4];
        ////
        UIButton *button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button5 addTarget:self
                    action:@selector(pushSeguro:)
          forControlEvents:UIControlEventTouchDown];
        
        [button5 setBackgroundImage:[UIImage imageNamed:@"Contactar-aseguradoraF.png"] forState:UIControlStateNormal];
        
        button5.frame = CGRectMake(20.0, 404.0, 280.0, 75.0);
        self.aseguradora = button5;
        [self.view addSubview:button5];

    }else{
        
        UIGraphicsBeginImageContext(self.pickerView.frame.size);
        [[UIImage imageNamed:@"fondo-gasolineriasF.png"] drawInRect:(CGRectMake(0, 0, 320, 416))];
        UIImage *image5 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.pickerView.backgroundColor = [UIColor colorWithPatternImage:image5];
        /////
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-información-vehicular.png"] drawInRect:(CGRectMake(0, 0, 320, 420))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(pushDemo:)
          forControlEvents:UIControlEventTouchDown];
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"Numero-de-pólizaF.png"] forState:UIControlStateNormal];
        
        button1.frame = CGRectMake(20.0, 64.0, 280.0, 75.0);
        self.poliza = button1;
        [self.view addSubview:button1];
        /////
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button2 addTarget:self
                    action:@selector(pushDemo1:)
          forControlEvents:UIControlEventTouchDown];
        
        [button2 setBackgroundImage:[UIImage imageNamed:@"Marca-del-vehículoF.png"] forState:UIControlStateNormal];
        
        button2.frame = CGRectMake(20.0, 134.0, 280.0, 75.0);
        self.marca = button2;
        [self.view addSubview:button2];
        /////
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button3 addTarget:self
                    action:@selector(pushDemo2:)
          forControlEvents:UIControlEventTouchDown];
        
        [button3 setBackgroundImage:[UIImage imageNamed:@"Modelo-del-vehículoF.png"] forState:UIControlStateNormal];
        
        button3.frame = CGRectMake(20.0, 204.0, 280.0, 75.0);
        self.modelo =button3;
        [self.view addSubview:button3];
/////
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button4 addTarget:self
                    action:@selector(pushDemo3:)
          forControlEvents:UIControlEventTouchDown];
        
        [button4 setBackgroundImage:[UIImage imageNamed:@"Año-del-vehículoF.png"] forState:UIControlStateNormal];
        
        button4.frame = CGRectMake(20.0, 274.0, 280.0, 75.0);
        self.ano = button4;
        [self.view addSubview:button4];
////
        UIButton *button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button5 addTarget:self
                    action:@selector(pushSeguro:)
          forControlEvents:UIControlEventTouchDown];
        
        [button5 setBackgroundImage:[UIImage imageNamed:@"Contactar-aseguradoraF.png"] forState:UIControlStateNormal];
        
        button5.frame = CGRectMake(20.0, 344.0, 280.0, 75.0);
        self.aseguradora = button5;
        [self.view addSubview:button5];


    }
    
    poliza.alpha = 0;
    marca.alpha = 0;
    modelo.alpha = 0;
    ano.alpha = 0;
    aseguradora.alpha = 0;
    

    
     [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(configV:) userInfo:nil repeats:NO];
    

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated{

    [poliza setTitle:[[NSUserDefaults standardUserDefaults]
                      stringForKey:@"poliza"] forState:UIControlStateNormal];
    [marca setTitle:[[NSUserDefaults standardUserDefaults]
                     stringForKey:@"marca"] forState:UIControlStateNormal];
    [modelo setTitle:[[NSUserDefaults standardUserDefaults]
                      stringForKey:@"modelo"] forState:UIControlStateNormal];
    [ano setTitle:[[NSUserDefaults standardUserDefaults]
                   stringForKey:@"ano"] forState:UIControlStateNormal];

}

-(void)viewWillDisappear:(BOOL)animated{
    
    poliza.alpha = 0;
    marca.alpha = 0;
    modelo.alpha = 0;
    ano.alpha = 0;
    aseguradora.alpha = 0;
    
    pickerView.alpha =  1;
    
    
}

-(IBAction)pushSeguro:(id)sender{
    pickerCount = 1;
    [brandPicker reloadAllComponents];
    [brandPicker reloadInputViews];
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
  //  CGRect headerFrame = pickerView.frame;
	
  //  headerFrame.origin.x -= 320;
	//pickerView.frame = headerFrame;
    
    poliza.alpha = 0;
    marca.alpha = 0;
    modelo.alpha = 0;
    ano.alpha = 0;
    aseguradora.alpha = 0;
    
     pickerView.alpha =  1;
    
    [UIView commitAnimations];
    
    llamar.hidden = NO;
}

-(IBAction)call:(id)sender{
    NSInteger row = [brandPicker selectedRowInComponent:0];
    
    NSString *phoneST = [[NSString alloc]initWithFormat:@"tel:%@", [self.numArray objectAtIndex:row]];
    
    NSURL *phoneNumberURL = [NSURL URLWithString:phoneST];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
    
    NSLog(@"%@, %@", phoneST, phoneNumberURL);

    
}

-(IBAction)pushWeb:(id)sender{
     WebController *showv = [[WebController alloc]initWithNibName:@"WebController" bundle:[NSBundle mainBundle]];
    
    if (pickerCount == 0) {
  
    NSInteger row = [brandPicker selectedRowInComponent:0];
    
    NSString *urlST = [[NSString alloc]initWithFormat:@"%@", [self.urlArray objectAtIndex:row]];
   
    showv.urlString = urlST;
    self.webCont = showv;
    }else{
        NSInteger row = [brandPicker selectedRowInComponent:0];
        
        NSString *urlST = [[NSString alloc]initWithFormat:@"%@", [self.urlSegArray objectAtIndex:row]];
        
        showv.urlString = urlST;
        self.webCont = showv;
    }
    [self.navigationController pushViewController:showv animated:YES];
    
   


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)gotoBack:(id)sender
{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"fondo-hidro-registro.png"] drawInRect:(CGRectMake(0, -62, 320, 480))];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    ///////
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)pushDemo:(id)sender{
    
        seleCount = 0;
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Número de Póliza", @"new_list_dialog")
                                                          message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [myAlertView show];
}
-(IBAction)pushDemo1:(id)sender{
    
    seleCount = 1;
    pickerCount = 0;
    [brandPicker reloadAllComponents];
    [brandPicker reloadInputViews];
       
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    poliza.alpha = 0;
    marca.alpha = 0;
    modelo.alpha = 0;
    ano.alpha = 0;
    aseguradora.alpha = 0;
    
     pickerView.alpha =  1;
    
    [UIView commitAnimations];
}
-(IBAction)pushDemo2:(id)sender{
        seleCount = 2;
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Modelo del Vehículo", @"new_list_dialog")
                                                          message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];

     myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
     [myAlertView show];
}
-(IBAction)pushDemo3:(id)sender{
    
        seleCount = 3;
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Año del Vehículo", @"new_list_dialog")
                                                          message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
     myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
     [myAlertView show];
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  //   NSString *string = [alertView textFieldAtIndex:0].text;
    
    if (buttonIndex == 1) {
   //
        
    NSLog(@"string entered=%@",[alertView textFieldAtIndex:0].text);
        
        NSString *string = [alertView textFieldAtIndex:0].text;
    //    NSString *string =
    
    if (seleCount == 0) {[poliza setTitle:string forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults]
         setObject:string forKey:@"poliza"];
    }
     if (seleCount == 1) { [marca setTitle:string forState:UIControlStateNormal];
         [[NSUserDefaults standardUserDefaults]
          setObject:string forKey:@"marca"];
     }
     if (seleCount == 2) { [modelo setTitle:string forState:UIControlStateNormal];
         [[NSUserDefaults standardUserDefaults]
          setObject:string forKey:@"modelo"];
     }
     if (seleCount == 3) { [ano setTitle:string forState:UIControlStateNormal];
         [[NSUserDefaults standardUserDefaults]
          setObject:string forKey:@"ano"];}
        
    }
    
}

-(IBAction)elegirMarca:(id)sender{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    poliza.alpha = 1;
    marca.alpha = 1;
    modelo.alpha = 1;
    ano.alpha = 1;
    aseguradora.alpha = 1;
    
     pickerView.alpha =  0;

    
    [UIView commitAnimations];
    
    NSInteger row = [brandPicker selectedRowInComponent:0];
    if(pickerCount == 0){
    NSString *carSt = [[NSString alloc]initWithFormat:@"%@", [self.brandArray objectAtIndex:row]];
    [marca setTitle:carSt forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setObject:carSt forKey:@"marca"];
    }else{
        NSString *carSt = [[NSString alloc]initWithFormat:@"%@", [self.seguroArray objectAtIndex:row]];
        [aseguradora setTitle:carSt forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setObject:carSt forKey:@"marca"];
        
            llamar.hidden = YES;

    }



}
-(IBAction)cancel{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    //CGRect headerFrame = pickerView.frame;
	  //  headerFrame.origin.x += 320;
	//pickerView.frame = headerFrame;
    
    poliza.alpha = 1;
    marca.alpha = 1;
    modelo.alpha = 1;
    ano.alpha = 1;
    aseguradora.alpha = 1;
    
     pickerView.alpha =  0;
    [UIView commitAnimations];
    
    llamar.hidden = YES;
}

-(IBAction)configV:(id)sender{

    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
	    poliza.alpha = 1;
    marca.alpha = 1;
    modelo.alpha = 1;
    ano.alpha = 1;
    aseguradora.alpha = 1;
    
    [UIView commitAnimations];
    NSLog(@"done");
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	
    if(pickerCount == 0){
        return [self.brandArray count];}
    else{
        return [self.seguroArray count];
    }
}
#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
     if(pickerCount == 0){
	return [self.brandArray objectAtIndex:row];
     }
     else{
        return [self.seguroArray objectAtIndex:row];
     }
}


@end
