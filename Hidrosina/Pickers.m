//
//  Pickers.m
//  Hidrosina
//
//  Created by Leopoldo G Vargas on 2013-04-26.
//  Copyright (c) 2013 Leopoldo G Vargas. All rights reserved.
//

#import "Pickers.h"

@interface Pickers ()

@end

@implementation Pickers
@synthesize brandArray, urlSegArray, urlArray, brandPicker, segPicker, seguroArray, webCont;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
