//
//  NotificationController.h
//  Hidrosina WatchKit Extension
//
//  Created by Leopoldo G Vargas on 2014-11-18.
//  Copyright (c) 2014 Leopoldo G Vargas. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface NotificationController : WKUserNotificationInterfaceController

@end
